import "office-ui-fabric-react/dist/css/fabric.min.css";
import App from "../src/components/App";
import {AppContainer} from "react-hot-loader";
import {initializeIcons} from "office-ui-fabric-react/lib/Icons";
import * as React from "react";
import * as ReactDOM from "react-dom";

initializeIcons();

let isOfficeInitialized = false;

const render = (Component) => {
    ReactDOM.render(
        <AppContainer>
            <Component isOfficeInitialized={isOfficeInitialized}/>
        </AppContainer>,
        document.getElementById("container")
    );
};

/* Render application after Office initializes */
Office.initialize = () => {
    isOfficeInitialized = true;

    render(App);
};


if ((module as any).hot) {
    (module as any).hot.accept("../src/components/App", () => {
        const NextApp = require("../src/components/App").default;
        render(NextApp);
    });
}
