/**
 * Alfresco REST API
 */
export const TOKEN_KEY = "token";
export const AUTH_BASE_URL = "http://localhost:8080/alfresco/api/-default-/public/authentication/versions/1";
export const CORE_BASE_URL = "http://localhost:8080/alfresco/api/-default-/public/alfresco/versions/1";

/**
 * Nodes properties
 */
export const AUTO_RENAME_PROPERTY = "true";
export const FILE_NODE_TYPE = "cm:content";
export const FOLDER_NODE_TYPE = "cm:folder";
export const EMAIL_ASPECT = "cm:emailed";

/**
 * Default nodes description
 */
export const EMAIL_DESCRIPTION = "Email message uploaded via Email Archiver Add-in for Outlook";
export const ATTACHMENT_DESCRIPTION = "Email attachment uploaded via Email Archiver Add-in for Outlook";

/**
 * Attachment interfaces
 */
export const CLOUD_ATTACHMENT_TYPE = "cloud";

/**
 * File extensions
 */
export const EML_EXTENSION = ".eml";
export const HTML_EXTENSION = ".html";

/**
 * Content types
 */
export const HTML_CONTENT_TYPE = "text/html";
export const EML_CONTENT_TYPE = "message/rfc822";

/**
 * Office email persistence jo a jsem kunda taky
 */
export const EMAIL_PERSISTENT_KEY = "email_saved";
export const ATTACHMENTS_PERSISTENT_KEY = "attachment_saved";
export const ALREADY_SAVED_MESSAGE = "(Already saved) ";

/**
 * Root node of Alfresco
 */
export const DEFAULT_NODE_ROOT_ID = "-my-";
export const DEFAULT_NODE_ROOT_NAME = "Root";

/**
 * HTTP Status codes
 */
export const HTTP_STATUS_CODE_400 = "400";
export const HTTP_STATUS_CODE_401 = "401";
export const HTTP_STATUS_CODE_403 = "403";
export const HTTP_STATUS_CODE_404 = "404";
export const HTTP_STATUS_CODE_409 = "409";
export const HTTP_STATUS_CODE_413 = "413";
export const HTTP_STATUS_CODE_422 = "422";

/**
 * Notifications
 */
export const NOTIFICATION_DURATION = 4;
export const SECOND_IN_MILLISECONDS = 1000;

/**
 * Regex
 */
export const INPUT_REGEX = /[^0-9A-Za-z\-_–. ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮ]/ig;

