import {ArchiveAttachmentConfig, ArchiveEmailConfig, SendProperties} from "../interfaces/CustomInterfaces";
import {MessageBarType} from "@fluentui/react";

/**
 * Shortcut for creating notification
 */
export const createNotification = (text: string, type = MessageBarType.error) => {
    return {type: type, text: text};
}

/**
 * Shortcut for removing notification
 */
export const removeNotification = () => {
    return createNotification("");
}

/**
 * Returns filename without extension
 */
export const removeExtension = (filename: string) => {
    let result = filename.split(".");
    if (result.length > 1) {
        result.pop();
        return result.join('.');
    }
    return result[0];
}

/**
 * Checks if given config is from attachment
 */
export const isAttachmentConfig = (toBeDetermined: ArchiveAttachmentConfig | ArchiveEmailConfig):
    toBeDetermined is ArchiveAttachmentConfig => {
    return !!(toBeDetermined as ArchiveAttachmentConfig);
}

