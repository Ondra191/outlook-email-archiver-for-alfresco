export interface TicketEntry {
    id: string;
    userId: string;
}

export interface Ticket {
    entry: TicketEntry;
}

export interface nodesResponse {
    list: NodeChildAssociationPaging
}

/**
 *
 * @export
 * @interface ActionDefinition
 */
export interface ActionDefinition {
    /**
     * Identifier of the action definition â€” used for example when executing an action
     * @type {string}
     * @memberof ActionDefinition
     */
    id: string;
    /**
     * name of the action definition, e.g. \"move\"
     * @type {string}
     * @memberof ActionDefinition
     */
    name?: string;
    /**
     * title of the action definition, e.g. \"Move\"
     * @type {string}
     * @memberof ActionDefinition
     */
    title?: string;
    /**
     * describes the action definition, e.g. \"This will move the matched item to another space.\"
     * @type {string}
     * @memberof ActionDefinition
     */
    description?: string;
    /**
     * QNames of the interfaces this action applies to
     * @type {Array<string>}
     * @memberof ActionDefinition
     */
    applicableTypes: Array<string>;
    /**
     * whether the basic action definition supports action tracking or not
     * @type {boolean}
     * @memberof ActionDefinition
     */
    trackStatus: boolean;
    /**
     *
     * @type {Array<ActionParameterDefinition>}
     * @memberof ActionDefinition
     */
    parameterDefinitions?: Array<ActionParameterDefinition>;
}

/**
 *
 * @export
 * @interface ActionDefinitionEntry
 */
export interface ActionDefinitionEntry {
    /**
     *
     * @type {ActionDefinition}
     * @memberof ActionDefinitionEntry
     */
    entry: ActionDefinition;
}

/**
 *
 * @export
 * @interface ActionDefinitionList
 */
export interface ActionDefinitionList {
    /**
     *
     * @type {any}
     * @memberof ActionDefinitionList
     */
    list?: any;
}

/**
 *
 * @export
 * @interface ActionExecResult
 */
export interface ActionExecResult {
    /**
     * The unique identifier of the action pending execution
     * @type {string}
     * @memberof ActionExecResult
     */
    id: string;
}

/**
 *
 * @export
 * @interface ActionExecResultEntry
 */
export interface ActionExecResultEntry {
    /**
     *
     * @type {ActionExecResult}
     * @memberof ActionExecResultEntry
     */
    entry: ActionExecResult;
}

/**
 *
 * @export
 * @interface ActionParameterDefinition
 */
export interface ActionParameterDefinition {
    /**
     *
     * @type {string}
     * @memberof ActionParameterDefinition
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof ActionParameterDefinition
     */
    type?: string;
    /**
     *
     * @type {boolean}
     * @memberof ActionParameterDefinition
     */
    multiValued?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ActionParameterDefinition
     */
    mandatory?: boolean;
    /**
     *
     * @type {string}
     * @memberof ActionParameterDefinition
     */
    displayLabel?: string;
}

/**
 * Activities describe any past activity in a site, for example creating an item of content, commenting on a node, liking an item of content.
 * @export
 * @interface Activity
 */
export interface Activity {
    /**
     * The id of the person who performed the activity
     * @type {string}
     * @memberof Activity
     */
    postPersonId: string;
    /**
     * The unique id of the activity
     * @type {number}
     * @memberof Activity
     */
    id: number;
    /**
     * The unique id of the site on which the activity was performed
     * @type {string}
     * @memberof Activity
     */
    siteId?: string;
    /**
     * The date time at which the activity was performed
     * @type {Date}
     * @memberof Activity
     */
    postedAt?: Date;
    /**
     * The feed on which this activity was posted
     * @type {string}
     * @memberof Activity
     */
    feedPersonId: string;
    /**
     * An object summarizing the activity
     * @type {{ [key: string]: string; }}
     * @memberof Activity
     */
    activitySummary?: { [key: string]: string; };
    /**
     * The type of the activity posted
     * @type {string}
     * @memberof Activity
     */
    activityType: string;
}

/**
 *
 * @export
 * @interface ActivityEntry
 */
export interface ActivityEntry {
    /**
     *
     * @type {Activity}
     * @memberof ActivityEntry
     */
    entry: Activity;
}

/**
 *
 * @export
 * @interface ActivityPaging
 */
export interface ActivityPaging {
    /**
     *
     * @type {any}
     * @memberof ActivityPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface Association
 */
export interface Association {
    /**
     *
     * @type {string}
     * @memberof Association
     */
    targetId: string;
    /**
     *
     * @type {string}
     * @memberof Association
     */
    assocType: string;
}

/**
 *
 * @export
 * @interface AssociationBody
 */
export interface AssociationBody {
    /**
     *
     * @type {string}
     * @memberof AssociationBody
     */
    targetId: string;
    /**
     *
     * @type {string}
     * @memberof AssociationBody
     */
    assocType: string;
}

/**
 *
 * @export
 * @interface AssociationEntry
 */
export interface AssociationEntry {
    /**
     *
     * @type {Association}
     * @memberof AssociationEntry
     */
    entry: Association;
}

/**
 *
 * @export
 * @interface AssociationInfo
 */
export interface AssociationInfo {
    /**
     *
     * @type {string}
     * @memberof AssociationInfo
     */
    assocType: string;
}

/**
 *
 * @export
 * @interface AuditApp
 */
export interface AuditApp {
    /**
     *
     * @type {string}
     * @memberof AuditApp
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof AuditApp
     */
    name?: string;
    /**
     *
     * @type {boolean}
     * @memberof AuditApp
     */
    isEnabled?: boolean;
    /**
     *
     * @type {number}
     * @memberof AuditApp
     */
    maxEntryId?: number;
    /**
     *
     * @type {number}
     * @memberof AuditApp
     */
    minEntryId?: number;
}

/**
 *
 * @export
 * @interface AuditAppEntry
 */
export interface AuditAppEntry {
    /**
     *
     * @type {AuditApp}
     * @memberof AuditAppEntry
     */
    entry?: AuditApp;
}

/**
 *
 * @export
 * @interface AuditAppPaging
 */
export interface AuditAppPaging {
    /**
     *
     * @type {any}
     * @memberof AuditAppPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface AuditBodyUpdate
 */
export interface AuditBodyUpdate {
    /**
     *
     * @type {boolean}
     * @memberof AuditBodyUpdate
     */
    isEnabled?: boolean;
}

/**
 *
 * @export
 * @interface AuditEntry
 */
export interface AuditEntry {
    /**
     *
     * @type {string}
     * @memberof AuditEntry
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof AuditEntry
     */
    auditApplicationId: string;
    /**
     *
     * @type {UserInfo}
     * @memberof AuditEntry
     */
    createdByUser: UserInfo;
    /**
     *
     * @type {Date}
     * @memberof AuditEntry
     */
    createdAt: Date;
    /**
     *
     * @type {any}
     * @memberof AuditEntry
     */
    values?: any;
}

/**
 *
 * @export
 * @interface AuditEntryEntry
 */
export interface AuditEntryEntry {
    /**
     *
     * @type {AuditEntry}
     * @memberof AuditEntryEntry
     */
    entry?: AuditEntry;
}

/**
 *
 * @export
 * @interface AuditEntryPaging
 */
export interface AuditEntryPaging {
    /**
     *
     * @type {any}
     * @memberof AuditEntryPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface Capabilities
 */
export interface Capabilities {
    /**
     *
     * @type {boolean}
     * @memberof Capabilities
     */
    isAdmin?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Capabilities
     */
    isGuest?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Capabilities
     */
    isMutable?: boolean;
}

/**
 *
 * @export
 * @interface ChildAssociation
 */
export interface ChildAssociation {
    /**
     *
     * @type {string}
     * @memberof ChildAssociation
     */
    childId: string;
    /**
     *
     * @type {string}
     * @memberof ChildAssociation
     */
    assocType: string;
}

/**
 *
 * @export
 * @interface ChildAssociationBody
 */
export interface ChildAssociationBody {
    /**
     *
     * @type {string}
     * @memberof ChildAssociationBody
     */
    childId: string;
    /**
     *
     * @type {string}
     * @memberof ChildAssociationBody
     */
    assocType: string;
}

/**
 *
 * @export
 * @interface ChildAssociationEntry
 */
export interface ChildAssociationEntry {
    /**
     *
     * @type {ChildAssociation}
     * @memberof ChildAssociationEntry
     */
    entry: ChildAssociation;
}

/**
 *
 * @export
 * @interface ChildAssociationInfo
 */
export interface ChildAssociationInfo {
    /**
     *
     * @type {string}
     * @memberof ChildAssociationInfo
     */
    assocType: string;
    /**
     *
     * @type {boolean}
     * @memberof ChildAssociationInfo
     */
    isPrimary: boolean;
}

/**
 *
 * @export
 * @interface ClientBody
 */
export interface ClientBody {
    /**
     * the client name
     * @type {string}
     * @memberof ClientBody
     */
    client: string;
}

/**
 *
 * @export
 * @interface Comment
 */
export interface Comment {
    /**
     *
     * @type {string}
     * @memberof Comment
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof Comment
     */
    title: string;
    /**
     *
     * @type {string}
     * @memberof Comment
     */
    content: string;
    /**
     *
     * @type {Person}
     * @memberof Comment
     */
    createdBy: Person;
    /**
     *
     * @type {Date}
     * @memberof Comment
     */
    createdAt: Date;
    /**
     *
     * @type {boolean}
     * @memberof Comment
     */
    edited: boolean;
    /**
     *
     * @type {Person}
     * @memberof Comment
     */
    modifiedBy: Person;
    /**
     *
     * @type {Date}
     * @memberof Comment
     */
    modifiedAt: Date;
    /**
     *
     * @type {boolean}
     * @memberof Comment
     */
    canEdit: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Comment
     */
    canDelete: boolean;
}

/**
 *
 * @export
 * @interface CommentBody
 */
export interface CommentBody {
    /**
     *
     * @type {string}
     * @memberof CommentBody
     */
    content: string;
}

/**
 *
 * @export
 * @interface CommentEntry
 */
export interface CommentEntry {
    /**
     *
     * @type {Comment}
     * @memberof CommentEntry
     */
    entry: Comment;
}

/**
 *
 * @export
 * @interface CommentPaging
 */
export interface CommentPaging {
    /**
     *
     * @type {any}
     * @memberof CommentPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface Company
 */
export interface Company {
    /**
     *
     * @type {string}
     * @memberof Company
     */
    organization?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    address1?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    address2?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    address3?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    postcode?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    telephone?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    fax?: string;
    /**
     *
     * @type {string}
     * @memberof Company
     */
    email?: string;
}

/**
 *
 * @export
 * @interface Constraint
 */
export interface Constraint {
    /**
     *
     * @type {string}
     * @memberof Constraint
     */
    id: string;
    /**
     * the type of the constraint
     * @type {string}
     * @memberof Constraint
     */
    type?: string;
    /**
     * the human-readable constraint title
     * @type {string}
     * @memberof Constraint
     */
    title?: string;
    /**
     * the human-readable constraint description
     * @type {string}
     * @memberof Constraint
     */
    description?: string;
    /**
     *
     * @type {{ [key: string]: any; }}
     * @memberof Constraint
     */
    parameters?: { [key: string]: any; };
}

/**
 *
 * @export
 * @interface ContentInfo
 */
export interface ContentInfo {
    /**
     *
     * @type {string}
     * @memberof ContentInfo
     */
    mimeType: string;
    /**
     *
     * @type {string}
     * @memberof ContentInfo
     */
    mimeTypeName?: string;
    /**
     *
     * @type {number}
     * @memberof ContentInfo
     */
    sizeInBytes?: number;
    /**
     *
     * @type {string}
     * @memberof ContentInfo
     */
    encoding?: string;
}

/**
 *
 * @export
 * @interface Definition
 */
export interface Definition {
    /**
     * List of property definitions effective for this node as the result of combining the type with all aspects.
     * @type {Array<Property>}
     * @memberof Definition
     */
    properties?: Array<Property>;
}

/**
 *
 * @export
 * @interface DeletedNode
 */
export interface DeletedNode extends Node {
    /**
     *
     * @type {UserInfo}
     * @memberof DeletedNode
     */
    archivedByUser: UserInfo;
    /**
     *
     * @type {Date}
     * @memberof DeletedNode
     */
    archivedAt: Date;
}

/**
 *
 * @export
 * @interface DeletedNodeBodyRestore
 */
export interface DeletedNodeBodyRestore {
    /**
     *
     * @type {string}
     * @memberof DeletedNodeBodyRestore
     */
    targetParentId?: string;
    /**
     *
     * @type {string}
     * @memberof DeletedNodeBodyRestore
     */
    assocType?: string;
}

/**
 *
 * @export
 * @interface DeletedNodeEntry
 */
export interface DeletedNodeEntry {
    /**
     *
     * @type {DeletedNode}
     * @memberof DeletedNodeEntry
     */
    entry?: DeletedNode;
}

/**
 *
 * @export
 * @interface DeletedNodesPaging
 */
export interface DeletedNodesPaging {
    /**
     *
     * @type {any}
     * @memberof DeletedNodesPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface DirectAccessUrlBodyCreate
 */
export interface DirectAccessUrlBodyCreate {
    /**
     *
     * @type {Date}
     * @memberof DirectAccessUrlBodyCreate
     */
    expiresAt?: Date;
    /**
     * The length of time in seconds that the url is valid for.
     * @type {number}
     * @memberof DirectAccessUrlBodyCreate
     */
    validFor?: number;
}

/**
 *
 * @export
 * @interface Download
 */
export interface Download {
    /**
     * number of files added so far in the zip
     * @type {number}
     * @memberof Download
     */
    filesAdded?: number;
    /**
     * number of bytes added so far in the zip
     * @type {number}
     * @memberof Download
     */
    bytesAdded?: number;
    /**
     * the id of the download node
     * @type {string}
     * @memberof Download
     */
    id?: string;
    /**
     * the total number of files to be added in the zip
     * @type {number}
     * @memberof Download
     */
    totalFiles?: number;
    /**
     * the total number of bytes to be added in the zip
     * @type {number}
     * @memberof Download
     */
    totalBytes?: number;
    /**
     * the current status of the download node creation
     * @type {string}
     * @memberof Download
     */
    status?: Download.StatusEnum;
}

/**
 * @export
 * @namespace Download
 */
export namespace Download {
    /**
     * @export
     * @enum {string}
     */
    export enum StatusEnum {
        PENDING = <any>'PENDING',
        CANCELLED = <any>'CANCELLED',
        INPROGRESS = <any>'IN_PROGRESS',
        DONE = <any>'DONE',
        MAXCONTENTSIZEEXCEEDED = <any>'MAX_CONTENT_SIZE_EXCEEDED'
    }
}

/**
 *
 * @export
 * @interface DownloadBodyCreate
 */
export interface DownloadBodyCreate {
    /**
     *
     * @type {Array<string>}
     * @memberof DownloadBodyCreate
     */
    nodeIds: Array<string>;
}

/**
 *
 * @export
 * @interface DownloadEntry
 */
export interface DownloadEntry {
    /**
     *
     * @type {Download}
     * @memberof DownloadEntry
     */
    entry: Download;
}

/**
 * A favorite describes an Alfresco entity that a person has marked as a favorite. The target can be a site, file or folder.
 * @export
 * @interface Favorite
 */
export interface Favorite {
    /**
     * The guid of the object that is a favorite.
     * @type {string}
     * @memberof Favorite
     */
    targetGuid: string;
    /**
     * The time the object was made a favorite.
     * @type {Date}
     * @memberof Favorite
     */
    createdAt?: Date;
    /**
     *
     * @type {any}
     * @memberof Favorite
     */
    target: any;
    /**
     * A subset of the target favorite properties, system properties and properties already available in the target are excluded.
     * @type {any}
     * @memberof Favorite
     */
    properties?: any;
}

/**
 *
 * @export
 * @interface FavoriteBodyCreate
 */
export interface FavoriteBodyCreate {
    /**
     *
     * @type {any}
     * @memberof FavoriteBodyCreate
     */
    target: any;
}

/**
 *
 * @export
 * @interface FavoriteEntry
 */
export interface FavoriteEntry {
    /**
     *
     * @type {Favorite}
     * @memberof FavoriteEntry
     */
    entry: Favorite;
}

/**
 *
 * @export
 * @interface FavoritePaging
 */
export interface FavoritePaging {
    /**
     *
     * @type {any}
     * @memberof FavoritePaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface FavoriteSite
 */
export interface FavoriteSite {
    /**
     *
     * @type {string}
     * @memberof FavoriteSite
     */
    id: string;
}

/**
 *
 * @export
 * @interface FavoriteSiteBodyCreate
 */
export interface FavoriteSiteBodyCreate {
    /**
     *
     * @type {string}
     * @memberof FavoriteSiteBodyCreate
     */
    id: string;
}

/**
 *
 * @export
 * @interface FavoriteSiteEntry
 */
export interface FavoriteSiteEntry {
    /**
     *
     * @type {FavoriteSite}
     * @memberof FavoriteSiteEntry
     */
    entry: FavoriteSite;
}

/**
 *
 * @export
 * @interface Group
 */
export interface Group {
    /**
     *
     * @type {string}
     * @memberof Group
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof Group
     */
    displayName: string;
    /**
     *
     * @type {boolean}
     * @memberof Group
     */
    isRoot: boolean;
    /**
     *
     * @type {Array<string>}
     * @memberof Group
     */
    parentIds?: Array<string>;
    /**
     *
     * @type {Array<string>}
     * @memberof Group
     */
    zones?: Array<string>;
}

/**
 *
 * @export
 * @interface GroupBodyCreate
 */
export interface GroupBodyCreate {
    /**
     *
     * @type {string}
     * @memberof GroupBodyCreate
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof GroupBodyCreate
     */
    displayName: string;
    /**
     *
     * @type {Array<string>}
     * @memberof GroupBodyCreate
     */
    parentIds?: Array<string>;
}

/**
 *
 * @export
 * @interface GroupBodyUpdate
 */
export interface GroupBodyUpdate {
    /**
     *
     * @type {string}
     * @memberof GroupBodyUpdate
     */
    displayName: string;
}

/**
 *
 * @export
 * @interface GroupEntry
 */
export interface GroupEntry {
    /**
     *
     * @type {Group}
     * @memberof GroupEntry
     */
    entry: Group;
}

/**
 *
 * @export
 * @interface GroupMember
 */
export interface GroupMember {
    /**
     *
     * @type {string}
     * @memberof GroupMember
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof GroupMember
     */
    displayName: string;
    /**
     *
     * @type {string}
     * @memberof GroupMember
     */
    memberType: GroupMember.MemberTypeEnum;
}

/**
 * @export
 * @namespace GroupMember
 */
export namespace GroupMember {
    /**
     * @export
     * @enum {string}
     */
    export enum MemberTypeEnum {
        GROUP = <any>'GROUP',
        PERSON = <any>'PERSON'
    }
}

/**
 *
 * @export
 * @interface GroupMemberEntry
 */
export interface GroupMemberEntry {
    /**
     *
     * @type {GroupMember}
     * @memberof GroupMemberEntry
     */
    entry: GroupMember;
}

/**
 *
 * @export
 * @interface GroupMemberPaging
 */
export interface GroupMemberPaging {
    /**
     *
     * @type {any}
     * @memberof GroupMemberPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface GroupMembershipBodyCreate
 */
export interface GroupMembershipBodyCreate {
    /**
     *
     * @type {string}
     * @memberof GroupMembershipBodyCreate
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof GroupMembershipBodyCreate
     */
    memberType: GroupMembershipBodyCreate.MemberTypeEnum;
}

/**
 * @export
 * @namespace GroupMembershipBodyCreate
 */
export namespace GroupMembershipBodyCreate {
    /**
     * @export
     * @enum {string}
     */
    export enum MemberTypeEnum {
        GROUP = <any>'GROUP',
        PERSON = <any>'PERSON'
    }
}

/**
 *
 * @export
 * @interface GroupPaging
 */
export interface GroupPaging {
    /**
     *
     * @type {any}
     * @memberof GroupPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface ModelError
 */
export interface ModelError {
    /**
     *
     * @type {any}
     * @memberof ModelError
     */
    error: any;
}

/**
 * Limits and usage of each quota. A network will have quotas for File space, the number of sites in the network, the number of people in the network, and the number of network administrators
 * @export
 * @interface NetworkQuota
 */
export interface NetworkQuota {
    /**
     *
     * @type {string}
     * @memberof NetworkQuota
     */
    id: string;
    /**
     *
     * @type {number}
     * @memberof NetworkQuota
     */
    limit: number;
    /**
     *
     * @type {number}
     * @memberof NetworkQuota
     */
    usage: number;
}

/**
 *
 * @export
 * @interface Node
 */
export interface Node {
    /**
     *
     * @type {string}
     * @memberof Node
     */
    id: string;
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof Node
     */
    name: string;
    /**
     *
     * @type {string}
     * @memberof Node
     */
    nodeType: string;
    /**
     *
     * @type {boolean}
     * @memberof Node
     */
    isFolder: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Node
     */
    isFile: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Node
     */
    isLocked?: boolean;
    /**
     *
     * @type {Date}
     * @memberof Node
     */
    modifiedAt: Date;
    /**
     *
     * @type {UserInfo}
     * @memberof Node
     */
    modifiedByUser: UserInfo;
    /**
     *
     * @type {Date}
     * @memberof Node
     */
    createdAt: Date;
    /**
     *
     * @type {UserInfo}
     * @memberof Node
     */
    createdByUser: UserInfo;
    /**
     *
     * @type {string}
     * @memberof Node
     */
    parentId?: string;
    /**
     *
     * @type {boolean}
     * @memberof Node
     */
    isLink?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Node
     */
    isFavorite?: boolean;
    /**
     *
     * @type {ContentInfo}
     * @memberof Node
     */
    content?: ContentInfo;
    /**
     *
     * @type {Array<string>}
     * @memberof Node
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {any}
     * @memberof Node
     */
    properties?: any;
    /**
     *
     * @type {Array<string>}
     * @memberof Node
     */
    allowableOperations?: Array<string>;
    /**
     *
     * @type {PathInfo}
     * @memberof Node
     */
    path?: PathInfo;
    /**
     *
     * @type {PermissionsInfo}
     * @memberof Node
     */
    permissions?: PermissionsInfo;
    /**
     *
     * @type {Definition}
     * @memberof Node
     */
    definition?: Definition;
}

/**
 *
 * @export
 * @interface NodeAssociation
 */
export interface NodeAssociation extends Node {
    /**
     *
     * @type {AssociationInfo}
     * @memberof NodeAssociation
     */
    association?: AssociationInfo;
}

/**
 *
 * @export
 * @interface NodeAssociationEntry
 */
export interface NodeAssociationEntry {
    /**
     *
     * @type {NodeAssociation}
     * @memberof NodeAssociationEntry
     */
    entry: NodeAssociation;
}

/**
 *
 * @export
 * @interface NodeAssociationPaging
 */
export interface NodeAssociationPaging {
    /**
     *
     * @type {any}
     * @memberof NodeAssociationPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface NodeBodyCopy
 */
export interface NodeBodyCopy {
    /**
     *
     * @type {string}
     * @memberof NodeBodyCopy
     */
    targetParentId: string;
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof NodeBodyCopy
     */
    name?: string;
}

/**
 *
 * @export
 * @interface NodeBodyCreate
 */
export interface NodeBodyCreate {
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof NodeBodyCreate
     */
    name: string;
    /**
     *
     * @type {string}
     * @memberof NodeBodyCreate
     */
    nodeType: string;
    /**
     *
     * @type {Array<string>}
     * @memberof NodeBodyCreate
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {any}
     * @memberof NodeBodyCreate
     */
    properties?: any;
    /**
     *
     * @type {PermissionsBody}
     * @memberof NodeBodyCreate
     */
    permissions?: PermissionsBody;
    /**
     *
     * @type {Definition}
     * @memberof NodeBodyCreate
     */
    definition?: Definition;
    /**
     *
     * @type {string}
     * @memberof NodeBodyCreate
     */
    relativePath?: string;
    /**
     *
     * @type {any}
     * @memberof NodeBodyCreate
     */
    association?: any;
    /**
     *
     * @type {Array<ChildAssociationBody>}
     * @memberof NodeBodyCreate
     */
    secondaryChildren?: Array<ChildAssociationBody>;
    /**
     *
     * @type {Array<AssociationBody>}
     * @memberof NodeBodyCreate
     */
    targets?: Array<AssociationBody>;
}

/**
 *
 * @export
 * @interface NodeBodyLock
 */
export interface NodeBodyLock {
    /**
     *
     * @type {number}
     * @memberof NodeBodyLock
     */
    timeToExpire?: number;
    /**
     *
     * @type {string}
     * @memberof NodeBodyLock
     */
    type?: NodeBodyLock.TypeEnum;
    /**
     *
     * @type {string}
     * @memberof NodeBodyLock
     */
    lifetime?: NodeBodyLock.LifetimeEnum;
}

/**
 * @export
 * @namespace NodeBodyLock
 */
export namespace NodeBodyLock {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        ALLOWOWNERCHANGES = <any>'ALLOW_OWNER_CHANGES',
        FULL = <any>'FULL'
    }

    /**
     * @export
     * @enum {string}
     */
    export enum LifetimeEnum {
        PERSISTENT = <any>'PERSISTENT',
        EPHEMERAL = <any>'EPHEMERAL'
    }
}

/**
 *
 * @export
 * @interface NodeBodyMove
 */
export interface NodeBodyMove {
    /**
     *
     * @type {string}
     * @memberof NodeBodyMove
     */
    targetParentId: string;
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof NodeBodyMove
     */
    name?: string;
}

/**
 *
 * @export
 * @interface NodeBodyUpdate
 */
export interface NodeBodyUpdate {
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof NodeBodyUpdate
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof NodeBodyUpdate
     */
    nodeType?: string;
    /**
     *
     * @type {Array<string>}
     * @memberof NodeBodyUpdate
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {{ [key: string]: string; }}
     * @memberof NodeBodyUpdate
     */
    properties?: { [key: string]: string; };
    /**
     *
     * @type {PermissionsBody}
     * @memberof NodeBodyUpdate
     */
    permissions?: PermissionsBody;
}

/**
 *
 * @export
 * @interface NodeChildAssociation
 */
export interface NodeChildAssociation extends Node {
    /**
     *
     * @type {ChildAssociationInfo}
     * @memberof NodeChildAssociation
     */
    association?: ChildAssociationInfo;
}

/**
 *
 * @export
 * @interface NodeChildAssociationEntry
 */
export interface NodeChildAssociationEntry {
    /**
     *
     * @type {NodeChildAssociation}
     * @memberof NodeChildAssociationEntry
     */
    entry: NodeChildAssociation;
}

/**
 *
 * @export
 * @interface NodeChildAssociationPaging
 */
export interface NodeChildAssociationPaging {
    /**
     *
     * @type {any}
     * @memberof NodeChildAssociationPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface NodeEntry
 */
export interface NodeEntry {
    /**
     *
     * @type {Node}
     * @memberof NodeEntry
     */
    entry: Node;
}

/**
 *
 * @export
 * @interface NodePaging
 */
export interface NodePaging {
    /**
     *
     * @type {any}
     * @memberof NodePaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface Pagination
 */
export interface Pagination {
    /**
     * The number of objects in the entries array.
     * @type {number}
     * @memberof Pagination
     */
    count?: number;
    /**
     * A boolean value which is **true** if there are more entities in the collection beyond those in this response. A true value means a request with a larger value for the **skipCount** or the **maxItems** parameter will return more entities.
     * @type {boolean}
     * @memberof Pagination
     */
    hasMoreItems?: boolean;
    /**
     * An integer describing the total number of entities in the collection. The API might not be able to determine this value, in which case this property will not be present.
     * @type {number}
     * @memberof Pagination
     */
    totalItems?: number;
    /**
     * An integer describing how many entities exist in the collection before those included in this list. If there was no **skipCount** parameter then the default value is 0.
     * @type {number}
     * @memberof Pagination
     */
    skipCount?: number;
    /**
     * The value of the **maxItems** parameter used to generate this list. If there was no **maxItems** parameter then the default value is 100.
     * @type {number}
     * @memberof Pagination
     */
    maxItems?: number;
}

/**
 *
 * @export
 * @interface PasswordResetBody
 */
export interface PasswordResetBody {
    /**
     * the new password
     * @type {string}
     * @memberof PasswordResetBody
     */
    password: string;
    /**
     * the workflow id provided in the reset password email
     * @type {string}
     * @memberof PasswordResetBody
     */
    id: string;
    /**
     * the workflow key provided in the reset password email
     * @type {string}
     * @memberof PasswordResetBody
     */
    key: string;
}

/**
 *
 * @export
 * @interface PathElement
 */
export interface PathElement {
    /**
     *
     * @type {string}
     * @memberof PathElement
     */
    id?: string;
    /**
     *
     * @type {string}
     * @memberof PathElement
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof PathElement
     */
    nodeType?: string;
    /**
     *
     * @type {Array<string>}
     * @memberof PathElement
     */
    aspectNames?: Array<string>;
}

/**
 *
 * @export
 * @interface PathInfo
 */
export interface PathInfo {
    /**
     *
     * @type {Array<PathElement>}
     * @memberof PathInfo
     */
    elements?: Array<PathElement>;
    /**
     *
     * @type {string}
     * @memberof PathInfo
     */
    name?: string;
    /**
     *
     * @type {boolean}
     * @memberof PathInfo
     */
    isComplete?: boolean;
}

/**
 *
 * @export
 * @interface PermissionElement
 */
export interface PermissionElement {
    /**
     *
     * @type {string}
     * @memberof PermissionElement
     */
    authorityId?: string;
    /**
     *
     * @type {string}
     * @memberof PermissionElement
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof PermissionElement
     */
    accessStatus?: PermissionElement.AccessStatusEnum;
}

/**
 * @export
 * @namespace PermissionElement
 */
export namespace PermissionElement {
    /**
     * @export
     * @enum {string}
     */
    export enum AccessStatusEnum {
        ALLOWED = <any>'ALLOWED',
        DENIED = <any>'DENIED'
    }
}

/**
 *
 * @export
 * @interface PermissionsBody
 */
export interface PermissionsBody {
    /**
     *
     * @type {boolean}
     * @memberof PermissionsBody
     */
    isInheritanceEnabled?: boolean;
    /**
     *
     * @type {Array<PermissionElement>}
     * @memberof PermissionsBody
     */
    locallySet?: Array<PermissionElement>;
}

/**
 *
 * @export
 * @interface PermissionsInfo
 */
export interface PermissionsInfo {
    /**
     *
     * @type {boolean}
     * @memberof PermissionsInfo
     */
    isInheritanceEnabled?: boolean;
    /**
     *
     * @type {Array<PermissionElement>}
     * @memberof PermissionsInfo
     */
    inherited?: Array<PermissionElement>;
    /**
     *
     * @type {Array<PermissionElement>}
     * @memberof PermissionsInfo
     */
    locallySet?: Array<PermissionElement>;
    /**
     *
     * @type {Array<string>}
     * @memberof PermissionsInfo
     */
    settable?: Array<string>;
}

/**
 *
 * @export
 * @interface Person
 */
export interface Person {
    /**
     *
     * @type {string}
     * @memberof Person
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    firstName: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    lastName?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    displayName?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    description?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    avatarId?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    email: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    skypeId?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    googleId?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    instantMessageId?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    jobTitle?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    location?: string;
    /**
     *
     * @type {Company}
     * @memberof Person
     */
    company?: Company;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    mobile?: string;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    telephone?: string;
    /**
     *
     * @type {Date}
     * @memberof Person
     */
    statusUpdatedAt?: Date;
    /**
     *
     * @type {string}
     * @memberof Person
     */
    userStatus?: string;
    /**
     *
     * @type {boolean}
     * @memberof Person
     */
    enabled: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Person
     */
    emailNotificationsEnabled?: boolean;
    /**
     *
     * @type {Array<string>}
     * @memberof Person
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {any}
     * @memberof Person
     */
    properties?: any;
    /**
     *
     * @type {Capabilities}
     * @memberof Person
     */
    capabilities?: Capabilities;
}

/**
 *
 * @export
 * @interface PersonBodyCreate
 */
export interface PersonBodyCreate {
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    firstName: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    lastName?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    description?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    email: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    skypeId?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    googleId?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    instantMessageId?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    jobTitle?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    location?: string;
    /**
     *
     * @type {Company}
     * @memberof PersonBodyCreate
     */
    company?: Company;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    mobile?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    telephone?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    userStatus?: string;
    /**
     *
     * @type {boolean}
     * @memberof PersonBodyCreate
     */
    enabled?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof PersonBodyCreate
     */
    emailNotificationsEnabled?: boolean;
    /**
     *
     * @type {string}
     * @memberof PersonBodyCreate
     */
    password: string;
    /**
     *
     * @type {Array<string>}
     * @memberof PersonBodyCreate
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {any}
     * @memberof PersonBodyCreate
     */
    properties?: any;
}

/**
 *
 * @export
 * @interface PersonBodyUpdate
 */
export interface PersonBodyUpdate {
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    firstName?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    lastName?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    description?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    email?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    skypeId?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    googleId?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    instantMessageId?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    jobTitle?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    location?: string;
    /**
     *
     * @type {Company}
     * @memberof PersonBodyUpdate
     */
    company?: Company;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    mobile?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    telephone?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    userStatus?: string;
    /**
     *
     * @type {boolean}
     * @memberof PersonBodyUpdate
     */
    enabled?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof PersonBodyUpdate
     */
    emailNotificationsEnabled?: boolean;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    password?: string;
    /**
     *
     * @type {string}
     * @memberof PersonBodyUpdate
     */
    oldPassword?: string;
    /**
     *
     * @type {Array<string>}
     * @memberof PersonBodyUpdate
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {any}
     * @memberof PersonBodyUpdate
     */
    properties?: any;
}

/**
 *
 * @export
 * @interface PersonEntry
 */
export interface PersonEntry {
    /**
     *
     * @type {Person}
     * @memberof PersonEntry
     */
    entry: Person;
}

/**
 * A network is the group of users and sites that belong to an organization. Networks are organized by email domain. When a user signs up for an Alfresco account , their email domain becomes their Home Network.
 * @export
 * @interface PersonNetwork
 */
export interface PersonNetwork {
    /**
     * This network's unique id
     * @type {string}
     * @memberof PersonNetwork
     */
    id: string;
    /**
     * Is this the home network?
     * @type {boolean}
     * @memberof PersonNetwork
     */
    homeNetwork?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof PersonNetwork
     */
    isEnabled: boolean;
    /**
     *
     * @type {Date}
     * @memberof PersonNetwork
     */
    createdAt?: Date;
    /**
     *
     * @type {boolean}
     * @memberof PersonNetwork
     */
    paidNetwork?: boolean;
    /**
     *
     * @type {string}
     * @memberof PersonNetwork
     */
    subscriptionLevel?: PersonNetwork.SubscriptionLevelEnum;
    /**
     *
     * @type {Array<NetworkQuota>}
     * @memberof PersonNetwork
     */
    quotas?: Array<NetworkQuota>;
}

/**
 * @export
 * @namespace PersonNetwork
 */
export namespace PersonNetwork {
    /**
     * @export
     * @enum {string}
     */
    export enum SubscriptionLevelEnum {
        Free = <any>'Free',
        Standard = <any>'Standard',
        Enterprise = <any>'Enterprise'
    }
}

/**
 *
 * @export
 * @interface PersonNetworkEntry
 */
export interface PersonNetworkEntry {
    /**
     *
     * @type {PersonNetwork}
     * @memberof PersonNetworkEntry
     */
    entry: PersonNetwork;
}

/**
 *
 * @export
 * @interface PersonNetworkPaging
 */
export interface PersonNetworkPaging {
    /**
     *
     * @type {any}
     * @memberof PersonNetworkPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface PersonPaging
 */
export interface PersonPaging {
    /**
     *
     * @type {any}
     * @memberof PersonPaging
     */
    list?: any;
}

/**
 * A specific preference.
 * @export
 * @interface Preference
 */
export interface Preference {
    /**
     * The unique id of the preference
     * @type {string}
     * @memberof Preference
     */
    id: string;
    /**
     * The value of the preference. Note that this can be of any JSON type.
     * @type {string}
     * @memberof Preference
     */
    value?: string;
}

/**
 *
 * @export
 * @interface PreferenceEntry
 */
export interface PreferenceEntry {
    /**
     *
     * @type {Preference}
     * @memberof PreferenceEntry
     */
    entry: Preference;
}

/**
 *
 * @export
 * @interface PreferencePaging
 */
export interface PreferencePaging {
    /**
     *
     * @type {any}
     * @memberof PreferencePaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface ProbeEntry
 */
export interface ProbeEntry {
    /**
     *
     * @type {any}
     * @memberof ProbeEntry
     */
    entry: any;
}

/**
 *
 * @export
 * @interface Property
 */
export interface Property {
    /**
     *
     * @type {string}
     * @memberof Property
     */
    id: string;
    /**
     * the human-readable title
     * @type {string}
     * @memberof Property
     */
    title?: string;
    /**
     * the human-readable description
     * @type {string}
     * @memberof Property
     */
    description?: string;
    /**
     * the default value
     * @type {string}
     * @memberof Property
     */
    defaultValue?: string;
    /**
     * the name of the property type (e.g. d:text)
     * @type {string}
     * @memberof Property
     */
    dataType?: string;
    /**
     * define if the property is multi-valued
     * @type {boolean}
     * @memberof Property
     */
    isMultiValued?: boolean;
    /**
     * define if the property is mandatory
     * @type {boolean}
     * @memberof Property
     */
    isMandatory?: boolean;
    /**
     * define if the presence of mandatory properties is enforced
     * @type {boolean}
     * @memberof Property
     */
    isMandatoryEnforced?: boolean;
    /**
     * define if the property is system maintained
     * @type {boolean}
     * @memberof Property
     */
    isProtected?: boolean;
    /**
     * list of constraints defined for the property
     * @type {Array<Constraint>}
     * @memberof Property
     */
    constraints?: Array<Constraint>;
}

/**
 * A person can rate an item of content by liking it. They can also remove their like of an item of content. API methods exist to get a list of ratings and to add a new rating.
 * @export
 * @interface Rating
 */
export interface Rating {
    /**
     *
     * @type {string}
     * @memberof Rating
     */
    id: string;
    /**
     *
     * @type {any}
     * @memberof Rating
     */
    aggregate: any;
    /**
     *
     * @type {Date}
     * @memberof Rating
     */
    ratedAt?: Date;
    /**
     * The rating. The type is specific to the rating scheme, boolean for the likes and an integer for the fiveStar.
     * @type {string}
     * @memberof Rating
     */
    myRating?: string;
}

/**
 *
 * @export
 * @interface RatingBody
 */
export interface RatingBody {
    /**
     * The rating scheme type. Possible values are likes and fiveStar.
     * @type {string}
     * @memberof RatingBody
     */
    id: RatingBody.IdEnum;
    /**
     * The rating. The type is specific to the rating scheme, boolean for the likes and an integer for the fiveStar
     * @type {string}
     * @memberof RatingBody
     */
    myRating: string;
}

/**
 * @export
 * @namespace RatingBody
 */
export namespace RatingBody {
    /**
     * @export
     * @enum {string}
     */
    export enum IdEnum {
        Likes = <any>'likes',
        FiveStar = <any>'fiveStar'
    }
}

/**
 *
 * @export
 * @interface RatingEntry
 */
export interface RatingEntry {
    /**
     *
     * @type {Rating}
     * @memberof RatingEntry
     */
    entry: Rating;
}

/**
 *
 * @export
 * @interface RatingPaging
 */
export interface RatingPaging {
    /**
     *
     * @type {any}
     * @memberof RatingPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface Rendition
 */
export interface Rendition {
    /**
     *
     * @type {string}
     * @memberof Rendition
     */
    id?: string;
    /**
     *
     * @type {ContentInfo}
     * @memberof Rendition
     */
    content?: ContentInfo;
    /**
     *
     * @type {string}
     * @memberof Rendition
     */
    status?: Rendition.StatusEnum;
}

/**
 * @export
 * @namespace Rendition
 */
export namespace Rendition {
    /**
     * @export
     * @enum {string}
     */
    export enum StatusEnum {
        CREATED = <any>'CREATED',
        NOTCREATED = <any>'NOT_CREATED'
    }
}

/**
 *
 * @export
 * @interface RenditionBodyCreate
 */
export interface RenditionBodyCreate {
    /**
     *
     * @type {string}
     * @memberof RenditionBodyCreate
     */
    id: string;
}

/**
 *
 * @export
 * @interface RenditionEntry
 */
export interface RenditionEntry {
    /**
     *
     * @type {Rendition}
     * @memberof RenditionEntry
     */
    entry: Rendition;
}

/**
 *
 * @export
 * @interface RenditionPaging
 */
export interface RenditionPaging {
    /**
     *
     * @type {any}
     * @memberof RenditionPaging
     */
    list?: any;
}

/**
 *
 * @export
 * @interface RevertBody
 */
export interface RevertBody {
    /**
     *
     * @type {boolean}
     * @memberof RevertBody
     */
    majorVersion?: boolean;
    /**
     *
     * @type {string}
     * @memberof RevertBody
     */
    comment?: string;
}

/**
 *
 * @export
 * @interface SharedLink
 */
export interface SharedLink {
    /**
     *
     * @type {string}
     * @memberof SharedLink
     */
    id?: string;
    /**
     *
     * @type {Date}
     * @memberof SharedLink
     */
    expiresAt?: Date;
    /**
     *
     * @type {string}
     * @memberof SharedLink
     */
    nodeId?: string;
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof SharedLink
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof SharedLink
     */
    title?: string;
    /**
     *
     * @type {string}
     * @memberof SharedLink
     */
    description?: string;
    /**
     *
     * @type {Date}
     * @memberof SharedLink
     */
    modifiedAt?: Date;
    /**
     *
     * @type {UserInfo}
     * @memberof SharedLink
     */
    modifiedByUser?: UserInfo;
    /**
     *
     * @type {UserInfo}
     * @memberof SharedLink
     */
    sharedByUser?: UserInfo;
    /**
     *
     * @type {ContentInfo}
     * @memberof SharedLink
     */
    content?: ContentInfo;
    /**
     * The allowable operations for the Quickshare link itself. See allowableOperationsOnTarget for the allowable operations pertaining to the linked content node.
     * @type {Array<string>}
     * @memberof SharedLink
     */
    allowableOperations?: Array<string>;
    /**
     * The allowable operations for the content node being shared.
     * @type {Array<string>}
     * @memberof SharedLink
     */
    allowableOperationsOnTarget?: Array<string>;
    /**
     *
     * @type {boolean}
     * @memberof SharedLink
     */
    isFavorite?: boolean;
    /**
     * A subset of the target node's properties, system properties and properties already available in the SharedLink are excluded.
     * @type {any}
     * @memberof SharedLink
     */
    properties?: any;
    /**
     *
     * @type {Array<string>}
     * @memberof SharedLink
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {PathInfo}
     * @memberof SharedLink
     */
    path?: PathInfo;
}

/**
 *
 * @export
 * @interface SharedLinkBodyCreate
 */
export interface SharedLinkBodyCreate {
    /**
     *
     * @type {string}
     * @memberof SharedLinkBodyCreate
     */
    nodeId: string;
    /**
     *
     * @type {Date}
     * @memberof SharedLinkBodyCreate
     */
    expiresAt?: Date;
}

/**
 *
 * @export
 * @interface SharedLinkBodyEmail
 */
export interface SharedLinkBodyEmail {
    /**
     *
     * @type {string}
     * @memberof SharedLinkBodyEmail
     */
    client?: string;
    /**
     *
     * @type {string}
     * @memberof SharedLinkBodyEmail
     */
    message?: string;
    /**
     *
     * @type {string}
     * @memberof SharedLinkBodyEmail
     */
    locale?: string;
    /**
     *
     * @type {Array<string>}
     * @memberof SharedLinkBodyEmail
     */
    recipientEmails?: Array<string>;
}

/**
 *
 * @export
 * @interface SharedLinkEntry
 */
export interface SharedLinkEntry {
    /**
     *
     * @type {SharedLink}
     * @memberof SharedLinkEntry
     */
    entry: SharedLink;
}

/**
 *
 * @export
 * @interface SharedLinkPaging
 */
export interface SharedLinkPaging {
    /**
     *
     * @type {any}
     * @memberof SharedLinkPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface Site
 */
export interface Site {
    /**
     *
     * @type {string}
     * @memberof Site
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof Site
     */
    guid: string;
    /**
     *
     * @type {string}
     * @memberof Site
     */
    title: string;
    /**
     *
     * @type {string}
     * @memberof Site
     */
    description?: string;
    /**
     *
     * @type {string}
     * @memberof Site
     */
    visibility: Site.VisibilityEnum;
    /**
     *
     * @type {string}
     * @memberof Site
     */
    preset?: string;
    /**
     *
     * @type {string}
     * @memberof Site
     */
    role?: Site.RoleEnum;
}

/**
 * @export
 * @namespace Site
 */
export namespace Site {
    /**
     * @export
     * @enum {string}
     */
    export enum VisibilityEnum {
        PRIVATE = <any>'PRIVATE',
        MODERATED = <any>'MODERATED',
        PUBLIC = <any>'PUBLIC'
    }

    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        SiteConsumer = <any>'SiteConsumer',
        SiteCollaborator = <any>'SiteCollaborator',
        SiteContributor = <any>'SiteContributor',
        SiteManager = <any>'SiteManager'
    }
}

/**
 *
 * @export
 * @interface SiteBodyCreate
 */
export interface SiteBodyCreate {
    /**
     *
     * @type {string}
     * @memberof SiteBodyCreate
     */
    id?: string;
    /**
     *
     * @type {string}
     * @memberof SiteBodyCreate
     */
    title: string;
    /**
     *
     * @type {string}
     * @memberof SiteBodyCreate
     */
    description?: string;
    /**
     *
     * @type {string}
     * @memberof SiteBodyCreate
     */
    visibility: SiteBodyCreate.VisibilityEnum;
}

/**
 * @export
 * @namespace SiteBodyCreate
 */
export namespace SiteBodyCreate {
    /**
     * @export
     * @enum {string}
     */
    export enum VisibilityEnum {
        PUBLIC = <any>'PUBLIC',
        PRIVATE = <any>'PRIVATE',
        MODERATED = <any>'MODERATED'
    }
}

/**
 *
 * @export
 * @interface SiteBodyUpdate
 */
export interface SiteBodyUpdate {
    /**
     *
     * @type {string}
     * @memberof SiteBodyUpdate
     */
    title?: string;
    /**
     *
     * @type {string}
     * @memberof SiteBodyUpdate
     */
    description?: string;
    /**
     *
     * @type {string}
     * @memberof SiteBodyUpdate
     */
    visibility?: SiteBodyUpdate.VisibilityEnum;
}

/**
 * @export
 * @namespace SiteBodyUpdate
 */
export namespace SiteBodyUpdate {
    /**
     * @export
     * @enum {string}
     */
    export enum VisibilityEnum {
        PRIVATE = <any>'PRIVATE',
        MODERATED = <any>'MODERATED',
        PUBLIC = <any>'PUBLIC'
    }
}

/**
 *
 * @export
 * @interface SiteContainer
 */
export interface SiteContainer {
    /**
     *
     * @type {string}
     * @memberof SiteContainer
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof SiteContainer
     */
    folderId: string;
}

/**
 *
 * @export
 * @interface SiteContainerEntry
 */
export interface SiteContainerEntry {
    /**
     *
     * @type {SiteContainer}
     * @memberof SiteContainerEntry
     */
    entry: SiteContainer;
}

/**
 *
 * @export
 * @interface SiteContainerPaging
 */
export interface SiteContainerPaging {
    /**
     *
     * @type {any}
     * @memberof SiteContainerPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface SiteEntry
 */
export interface SiteEntry {
    /**
     *
     * @type {Site}
     * @memberof SiteEntry
     */
    entry: Site;
}

/**
 *
 * @export
 * @interface SiteGroup
 */
export interface SiteGroup {
    /**
     *
     * @type {string}
     * @memberof SiteGroup
     */
    id: string;
    /**
     *
     * @type {GroupMember}
     * @memberof SiteGroup
     */
    group: GroupMember;
    /**
     *
     * @type {string}
     * @memberof SiteGroup
     */
    role: SiteGroup.RoleEnum;
}

/**
 * @export
 * @namespace SiteGroup
 */
export namespace SiteGroup {
    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        SiteConsumer = <any>'SiteConsumer',
        SiteCollaborator = <any>'SiteCollaborator',
        SiteContributor = <any>'SiteContributor',
        SiteManager = <any>'SiteManager'
    }
}

/**
 *
 * @export
 * @interface SiteGroupEntry
 */
export interface SiteGroupEntry {
    /**
     *
     * @type {SiteGroup}
     * @memberof SiteGroupEntry
     */
    entry: SiteGroup;
}

/**
 *
 * @export
 * @interface SiteGroupPaging
 */
export interface SiteGroupPaging {
    /**
     *
     * @type {any}
     * @memberof SiteGroupPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface SiteMember
 */
export interface SiteMember {
    /**
     *
     * @type {string}
     * @memberof SiteMember
     */
    id: string;
    /**
     *
     * @type {Person}
     * @memberof SiteMember
     */
    person: Person;
    /**
     *
     * @type {string}
     * @memberof SiteMember
     */
    role: SiteMember.RoleEnum;
    /**
     *
     * @type {boolean}
     * @memberof SiteMember
     */
    isMemberOfGroup?: boolean;
}

/**
 * @export
 * @namespace SiteMember
 */
export namespace SiteMember {
    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        SiteConsumer = <any>'SiteConsumer',
        SiteCollaborator = <any>'SiteCollaborator',
        SiteContributor = <any>'SiteContributor',
        SiteManager = <any>'SiteManager'
    }
}

/**
 *
 * @export
 * @interface SiteMemberEntry
 */
export interface SiteMemberEntry {
    /**
     *
     * @type {SiteMember}
     * @memberof SiteMemberEntry
     */
    entry: SiteMember;
}

/**
 *
 * @export
 * @interface SiteMemberPaging
 */
export interface SiteMemberPaging {
    /**
     *
     * @type {any}
     * @memberof SiteMemberPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface SiteMembershipApprovalBody
 */
export interface SiteMembershipApprovalBody {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipApprovalBody
     */
    role?: string;
}

/**
 *
 * @export
 * @interface SiteMembershipBodyCreate
 */
export interface SiteMembershipBodyCreate {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipBodyCreate
     */
    role: SiteMembershipBodyCreate.RoleEnum;
    /**
     *
     * @type {string}
     * @memberof SiteMembershipBodyCreate
     */
    id: string;
}

/**
 * @export
 * @namespace SiteMembershipBodyCreate
 */
export namespace SiteMembershipBodyCreate {
    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        SiteConsumer = <any>'SiteConsumer',
        SiteCollaborator = <any>'SiteCollaborator',
        SiteContributor = <any>'SiteContributor',
        SiteManager = <any>'SiteManager'
    }
}

/**
 *
 * @export
 * @interface SiteMembershipBodyUpdate
 */
export interface SiteMembershipBodyUpdate {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipBodyUpdate
     */
    role: SiteMembershipBodyUpdate.RoleEnum;
}

/**
 * @export
 * @namespace SiteMembershipBodyUpdate
 */
export namespace SiteMembershipBodyUpdate {
    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        SiteConsumer = <any>'SiteConsumer',
        SiteCollaborator = <any>'SiteCollaborator',
        SiteContributor = <any>'SiteContributor',
        SiteManager = <any>'SiteManager'
    }
}

/**
 *
 * @export
 * @interface SiteMembershipRejectionBody
 */
export interface SiteMembershipRejectionBody {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRejectionBody
     */
    comment?: string;
}

/**
 *
 * @export
 * @interface SiteMembershipRequest
 */
export interface SiteMembershipRequest {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequest
     */
    id: string;
    /**
     *
     * @type {Date}
     * @memberof SiteMembershipRequest
     */
    createdAt: Date;
    /**
     *
     * @type {Site}
     * @memberof SiteMembershipRequest
     */
    site: Site;
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequest
     */
    message?: string;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestBodyCreate
 */
export interface SiteMembershipRequestBodyCreate {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequestBodyCreate
     */
    message?: string;
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequestBodyCreate
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequestBodyCreate
     */
    title?: string;
    /**
     * Optional client name used when sending an email to the end user, defaults to \"share\" if not provided. **Note:** The client must be registered before this API can send an email. **Note:** This is available in Alfresco 7.0.0 and newer versions.
     * @type {string}
     * @memberof SiteMembershipRequestBodyCreate
     */
    client?: string;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestBodyUpdate
 */
export interface SiteMembershipRequestBodyUpdate {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequestBodyUpdate
     */
    message?: string;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestEntry
 */
export interface SiteMembershipRequestEntry {
    /**
     *
     * @type {SiteMembershipRequest}
     * @memberof SiteMembershipRequestEntry
     */
    entry: SiteMembershipRequest;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestPaging
 */
export interface SiteMembershipRequestPaging {
    /**
     *
     * @type {any}
     * @memberof SiteMembershipRequestPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestWithPerson
 */
export interface SiteMembershipRequestWithPerson {
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequestWithPerson
     */
    id: string;
    /**
     *
     * @type {Date}
     * @memberof SiteMembershipRequestWithPerson
     */
    createdAt: Date;
    /**
     *
     * @type {Site}
     * @memberof SiteMembershipRequestWithPerson
     */
    site: Site;
    /**
     *
     * @type {Person}
     * @memberof SiteMembershipRequestWithPerson
     */
    person: Person;
    /**
     *
     * @type {string}
     * @memberof SiteMembershipRequestWithPerson
     */
    message?: string;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestWithPersonEntry
 */
export interface SiteMembershipRequestWithPersonEntry {
    /**
     *
     * @type {SiteMembershipRequestWithPerson}
     * @memberof SiteMembershipRequestWithPersonEntry
     */
    entry: SiteMembershipRequestWithPerson;
}

/**
 *
 * @export
 * @interface SiteMembershipRequestWithPersonPaging
 */
export interface SiteMembershipRequestWithPersonPaging {
    /**
     *
     * @type {any}
     * @memberof SiteMembershipRequestWithPersonPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface SitePaging
 */
export interface SitePaging {
    /**
     *
     * @type {any}
     * @memberof SitePaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface SiteRole
 */
export interface SiteRole {
    /**
     *
     * @type {Site}
     * @memberof SiteRole
     */
    site: Site;
    /**
     *
     * @type {string}
     * @memberof SiteRole
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof SiteRole
     */
    guid: string;
    /**
     *
     * @type {string}
     * @memberof SiteRole
     */
    role: SiteRole.RoleEnum;
}

/**
 * @export
 * @namespace SiteRole
 */
export namespace SiteRole {
    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        SiteConsumer = <any>'SiteConsumer',
        SiteCollaborator = <any>'SiteCollaborator',
        SiteContributor = <any>'SiteContributor',
        SiteManager = <any>'SiteManager'
    }
}

/**
 *
 * @export
 * @interface SiteRoleEntry
 */
export interface SiteRoleEntry {
    /**
     *
     * @type {SiteRole}
     * @memberof SiteRoleEntry
     */
    entry: SiteRole;
}

/**
 *
 * @export
 * @interface SiteRolePaging
 */
export interface SiteRolePaging {
    /**
     *
     * @type {any}
     * @memberof SiteRolePaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface Tag
 */
export interface Tag {
    /**
     *
     * @type {string}
     * @memberof Tag
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof Tag
     */
    tag: string;
    /**
     *
     * @type {number}
     * @memberof Tag
     */
    count?: number;
}

/**
 *
 * @export
 * @interface TagBody
 */
export interface TagBody {
    /**
     *
     * @type {string}
     * @memberof TagBody
     */
    tag: string;
}

/**
 *
 * @export
 * @interface TagEntry
 */
export interface TagEntry {
    /**
     *
     * @type {Tag}
     * @memberof TagEntry
     */
    entry: Tag;
}

/**
 *
 * @export
 * @interface TagPaging
 */
export interface TagPaging {
    /**
     *
     * @type {any}
     * @memberof TagPaging
     */
    list: any;
}

/**
 *
 * @export
 * @interface UserInfo
 */
export interface UserInfo {
    /**
     *
     * @type {string}
     * @memberof UserInfo
     */
    displayName: string;
    /**
     *
     * @type {string}
     * @memberof UserInfo
     */
    id: string;
}

/**
 *
 * @export
 * @interface Version
 */
export interface Version {
    /**
     *
     * @type {string}
     * @memberof Version
     */
    id: string;
    /**
     *
     * @type {string}
     * @memberof Version
     */
    versionComment?: string;
    /**
     * The name must not contain spaces or the following special characters: * \" < > \\ / ? : and |. The character . must not be used at the end of the name.
     * @type {string}
     * @memberof Version
     */
    name: string;
    /**
     *
     * @type {string}
     * @memberof Version
     */
    nodeType: string;
    /**
     *
     * @type {boolean}
     * @memberof Version
     */
    isFolder: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Version
     */
    isFile: boolean;
    /**
     *
     * @type {Date}
     * @memberof Version
     */
    modifiedAt: Date;
    /**
     *
     * @type {UserInfo}
     * @memberof Version
     */
    modifiedByUser: UserInfo;
    /**
     *
     * @type {ContentInfo}
     * @memberof Version
     */
    content?: ContentInfo;
    /**
     *
     * @type {Array<string>}
     * @memberof Version
     */
    aspectNames?: Array<string>;
    /**
     *
     * @type {any}
     * @memberof Version
     */
    properties?: any;
}

/**
 *
 * @export
 * @interface VersionEntry
 */
export interface VersionEntry {
    /**
     *
     * @type {Version}
     * @memberof VersionEntry
     */
    entry?: Version;
}

/**
 *
 * @export
 * @interface VersionPaging
 */
export interface VersionPaging {
    /**
     *
     * @type {any}
     * @memberof VersionPaging
     */
    list?: any;
}