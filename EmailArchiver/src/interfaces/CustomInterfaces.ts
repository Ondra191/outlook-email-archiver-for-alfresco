import {MessageBarType} from "@fluentui/react";

/**
 * Configuration of upload data for email
 */
export interface ArchiveEmailConfig {
    filedata?: any;
    filename: string;
    cmTitle: string;
    cmDescription?: string;
    sendProperties: SendProperties;
}

/**
 * Configuration of upload data for attachment
 */
export interface ArchiveAttachmentConfig extends ArchiveEmailConfig {
    contentType: string;
    attachmentType: string;
}

/**
 * Unique identifier of specific node
 */
export interface NodeIdentifier {
    id: string;
    name: string;
}

/**
 * Notification information
 */
export interface Notification {
    text: string;
    type: MessageBarType;
}

/**
 * Interface for attachment checkboxes in archiving view
 */
export interface AttachmentCheckboxIdentifier {
    id: string;
    checked: boolean;
    attachmentType: string;
    name: string;
    disabled: boolean;
    isSaved: boolean;
}

/**
 * Which properties to send with email or attachment
 */
export interface SendProperties {
    sendSubject: boolean;
    sendFrom: boolean;
    sendTo: boolean;
    sendToAll: boolean;
    sendTime: boolean;
}