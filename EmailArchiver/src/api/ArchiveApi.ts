import {coreAxios, requestConfig} from "./axiosSetup";
import {ArchiveAttachmentConfig, ArchiveEmailConfig} from "../interfaces/CustomInterfaces";
import {isAttachmentConfig} from "../common/Utils";
import {
    AUTO_RENAME_PROPERTY,
    EMAIL_ASPECT, EML_CONTENT_TYPE,
    EML_EXTENSION,
    FILE_NODE_TYPE,
    HTML_CONTENT_TYPE
} from "../common/Constants";
import {base64StringToBlob} from "blob-util";

/**
 * Uploads email to Alfresco
 */
export const archiveEmail = async (parentName: string, config: ArchiveEmailConfig) => {
    const path = "/nodes/" + parentName + "/children";

    const formData = createFormData(false, config);
    const response = await coreAxios.post(path, formData, requestConfig());

    return response.data.entry;
}

/**
 * Uploads given attachments to Alfresco
 */
export const archiveAttachment = async (parentName: string, config: ArchiveAttachmentConfig) => {
    const path = "/nodes/" + parentName + "/children";

    const formData = createFormData(true, config);
    const response = await coreAxios.post(path, formData, requestConfig());

    return response.data.entry;
}

/**
 * Creates FormData for sending emails or attachments data with properties
 */
const createFormData = (isAttachment: boolean, config: (ArchiveEmailConfig | ArchiveAttachmentConfig)) => {
    const formData = new FormData();

    formData.append("nodeType", FILE_NODE_TYPE);
    formData.append("autoRename", AUTO_RENAME_PROPERTY);
    formData.append("aspectNames", EMAIL_ASPECT);
    formData.append("cm:title", config.cmTitle);
    formData.append("cm:description", config.cmDescription);

    const whatToSend = config.sendProperties;
    const messageProperties = getMessageProperties();
    formData.append("cm:subjectline", whatToSend.sendSubject ? messageProperties.subject : "");
    formData.append("cm:originator", whatToSend.sendFrom ? messageProperties.from : "");
    formData.append("cm:addressee", whatToSend.sendTo ? Office.context.mailbox.userProfile.emailAddress : "");
    formData.append("cm:addressees", whatToSend.sendToAll ? messageProperties.to.join(", ") : "");
    if (whatToSend.sendTime) {
        formData.append("cm:sentdate", messageProperties.dateTimeCreated.toISOString());
    }

    let blob;
    if (isAttachment && isAttachmentConfig(config)) {
        blob = toBlob(config.attachmentType, config.filedata, config.contentType);

        if (config.attachmentType === Office.MailboxEnums.AttachmentContentFormat.Eml && !config.filename.endsWith(EML_EXTENSION)) {
            config.filename += EML_EXTENSION;
        }
    } else {
        blob = new Blob([config.filedata], {type: HTML_CONTENT_TYPE});
    }

    formData.append("filedata", blob);
    formData.append("name", config.filename);

    return formData;
}

/**
 * Gets properties of current email message viewed
 */
const getMessageProperties = () => {
    const item = Office.context.mailbox.item;

    return {
        "subject": item.subject,
        "dateTimeCreated": item.dateTimeCreated,
        "from": item.from.emailAddress,
        "to": item.to.map(v => v.emailAddress),
        "cc": item.cc.map(v => v.emailAddress),
        "attachments": item.attachments.map(v => v.name),
    }
}

/**
 * Converts given filedata to Binary large object
 */
const toBlob = (attachmentType: string, filedata: any, contentType: string | null | undefined) => {
    let errorMessage = "";
    switch (attachmentType) {
        case Office.MailboxEnums.AttachmentContentFormat.Base64:
        case Office.MailboxEnums.AttachmentContentFormat.ICalendar:
            return base64StringToBlob(filedata, contentType);
        case Office.MailboxEnums.AttachmentContentFormat.Eml:
            return base64StringToBlob(window.btoa(filedata), EML_CONTENT_TYPE);
        case Office.MailboxEnums.AttachmentContentFormat.Url:
            errorMessage = "Not supported type of attachment";
            console.error(errorMessage);
            throw new Error(errorMessage);
        default:
            errorMessage = "Attachment has unknown type: " + attachmentType;
            console.error(errorMessage);
            throw new Error(errorMessage);
    }
}
