import {coreAxios, requestConfig} from "./axiosSetup";
import {Node, NodeEntry} from "../interfaces/AlfrescoInterfaces";
import {FOLDER_NODE_TYPE} from "../common/Constants";

/**
 * Gets all children nodes of given parent folder
 */
export const getFolderChildren: (parentId: string) => Promise<Node[]> = async (parentId: string) => {
    const path = "/nodes/" + parentId + "/children";

    const response = await coreAxios.get(path, requestConfig());

    const entries: NodeEntry[] = response.data.list.entries;

    return entries.map(nodeEntry => nodeEntry.entry);
};

/**
 * Creates node of type folder in given parent folder with given name
 */
export const createFolder = async (parentId: string, newFolderName: string) => {
    const path = "/nodes/" + parentId + "/children";
    const data = {
        "name": newFolderName,
        "nodeType": FOLDER_NODE_TYPE
    }

    const response = await coreAxios.post(path, data, requestConfig());

    return response.data.entry;
};

/**
 * Renames given node
 */
export const renameNode: (nodeId: string, newName: string) => Promise<Node> = async (nodeId: string, newName: string) => {
    const path = "/nodes/" + nodeId;
    const data = {
        "name": newName
    };

    const response = await coreAxios.put(path, data, requestConfig())

    return response.data.entry;
};

/**
 * Removes given node
 */
export const removeNode: (nodeId: string) => Promise<number> = async (nodeId: string) => {
    const path = "/nodes/" + nodeId;

    const response = await coreAxios.delete(path, requestConfig());

    return response.status;
};
