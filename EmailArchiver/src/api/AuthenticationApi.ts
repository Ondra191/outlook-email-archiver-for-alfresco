import {authAxios, requestConfig} from "./axiosSetup";
import {AxiosResponse} from "axios";
import {TOKEN_KEY} from "../common/Constants";

/**
 * Obtains authentication token from alfresco for Login and HTTP requests to Alfresco
 */
export const obtainToken = async (userId: string, password: string) => {
    const requestBody = {
        "userId": userId,
        "password": password
    };
    const path = "/tickets";
    const response = await authAxios.post(path, requestBody)

    sessionStorage.setItem(TOKEN_KEY, response.data.entry.id);
}

/**
 * Returns Authorization HTTP header in format 'BASIC $token'
 */
export const getAuthHeader = () => {
    const token = sessionStorage.getItem(TOKEN_KEY);
    if (token === null) {
        return null;
    }
    return "BASIC " + window.btoa(token);
}

/**
 * Checks if the authentication token is valid
 */
export const isValidToken: () => Promise<number> = async () => {
    const path = "/tickets/-me-";
    const response: AxiosResponse = await authAxios.get(path, requestConfig());

    return response.status;
};

/**
 * Removes the authentication token
 */
export const removeToken = async () => {
    const path = "/tickets/-me-";
    await authAxios.delete(path, requestConfig()).catch(e => console.error(e));

    sessionStorage.removeItem(TOKEN_KEY);
};