import axios from "axios";
import {getAuthHeader} from "./authenticationApi";
import {AUTH_BASE_URL, CORE_BASE_URL} from "../common/Constants";

/**
 * Axios instance for Alfresco AUTH API calls
 */
export const authAxios = axios.create({
    baseURL: AUTH_BASE_URL
});

/**
 * Axios instance for Alfresco CORE API calls
 */
export const coreAxios = axios.create({
    baseURL: CORE_BASE_URL
});

/**
 * Default request configuration for Axios with auth header
 */
export const requestConfig = () => {
    return {
        headers: {
            Authorization: getAuthHeader()
        }
    };
};

