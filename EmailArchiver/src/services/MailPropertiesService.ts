import {loadCustomProperties, saveProperties} from "./OfficePromiseWrapService";

/**
 * Get custom persistent property by given key
 */
export const getPersistentEmailProperty = async (key: string) => {
    const result = await loadCustomProperties();
    return result.value.get(key);
}

/**
 * Save custom persistent property by given key and value
 */
export const savePersistentEmailProperty = async (key: string, value: any) => {
    const result = await loadCustomProperties();
    result.value.set(key, value);
    return await saveProperties(result);
}