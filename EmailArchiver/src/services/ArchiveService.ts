import {getAttachmentsContentAsync, getBodyAsync} from "./OfficePromiseWrapService";
import {ArchiveAttachmentConfig, ArchiveEmailConfig, SendProperties} from "../interfaces/CustomInterfaces";
import {archiveAttachment, archiveEmail} from "../api/ArchiveApi";
import {removeExtension} from "../common/Utils";
import {savePersistentEmailProperty} from "./MailPropertiesService";
import {Node} from "../interfaces/AlfrescoInterfaces";
import {
    ATTACHMENT_DESCRIPTION,
    ATTACHMENTS_PERSISTENT_KEY,
    EMAIL_DESCRIPTION,
    EMAIL_PERSISTENT_KEY, HTML_EXTENSION
} from "../common/Constants";

/**
 * Uploads email with wanted properties to Alfresco and saves persistent information about uploading
 */
export const saveEmail = async (parentName: string, emailConfig: ArchiveEmailConfig): Promise<Node> => {
    const body = await getBodyAsync(Office.CoercionType.Html);

    emailConfig = {
        ...emailConfig,
        filedata: body.value,
        cmDescription: EMAIL_DESCRIPTION
    };

    const newNode = await archiveEmail(parentName, emailConfig);

    await savePersistentEmailProperty(EMAIL_PERSISTENT_KEY, true);

    return newNode;
}

/**
 * Uploads all specified attachments with wanted properties to Alfresco
 * and saves persistent information about uploading of each one
 */
export const saveAttachments = async (parentName: string, sendProperties: SendProperties, attachmentIds?: string[]) => {
    const newNodes = [];

    for (const attachment of Office.context.mailbox.item.attachments) {
        if (attachmentIds && !attachmentIds.includes(attachment.id)) {
            continue;
        }

        const attachmentContent = await getAttachmentsContentAsync(attachment.id);

        const attachmentConfig: ArchiveAttachmentConfig = {
            filedata: attachmentContent.value.content,
            attachmentType: attachmentContent.value.format,
            filename: attachment.name,
            contentType: attachment.contentType,
            cmTitle: removeExtension(attachment.name),
            cmDescription: ATTACHMENT_DESCRIPTION,
            sendProperties: sendProperties
        }

        const node = await archiveAttachment(parentName, attachmentConfig);
        newNodes.push(node);

        await savePersistentEmailProperty(ATTACHMENTS_PERSISTENT_KEY + attachment.id, true);
    }

    return newNodes;
}

/**
 * Template for config of Archiving email
 */
export const createArchiveEmailConfig = (name, sendProperties: SendProperties) => {
    name = name ? name : "Email";
    return {
        filename: name + HTML_EXTENSION,
        cmTitle: name,
        sendProperties: sendProperties
    }
}

