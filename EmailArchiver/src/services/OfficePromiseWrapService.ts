import CustomProperties = Office.CustomProperties;

/**
 * Wrapper for Office.js : getting content from attachments of email
 */
export const getAttachmentsContentAsync: (detailId: string) => Promise<Office.AsyncResult<Office.AttachmentContent>> = (detailId: string) => {
    return new Promise((resolve, reject) => {
        Office.context.mailbox.item.getAttachmentContentAsync(detailId, (attachmentsResult) => {
            if (attachmentsResult.status === Office.AsyncResultStatus.Succeeded) {
                resolve(attachmentsResult);
            } else {
                reject(attachmentsResult);
            }
        });
    });
}

/**
 * Wrapper for Office.js : getting message body of email
 */
export const getBodyAsync: (coercionType: Office.CoercionType) => Promise<Office.AsyncResult<string>> = (coercionType: Office.CoercionType) => {
    return new Promise((resolve, reject) => {
        Office.context.mailbox.item.body.getAsync(coercionType, result => {
            if (result.status === Office.AsyncResultStatus.Succeeded) {
                resolve(result);
            } else {
                reject(result);
            }
        });
    });
}

/**
 * Wrapper for Office.js : loading custom persistent properties of current item (email)
 */
export const loadCustomProperties: () => Promise<Office.AsyncResult<CustomProperties>> = () => {
    return new Promise((resolve, reject) => {
        Office.context.mailbox.item.loadCustomPropertiesAsync(result => {
            if (result.status === Office.AsyncResultStatus.Succeeded) {
                resolve(result);
            } else {
                reject(result);
            }
        });
    });
}

/**
 * Wrapper for Office.js : saving custom persistent properties of current item (email)
 */
export const saveProperties: (result) => Promise<Office.AsyncResult<void>> = (result: Office.AsyncResult<CustomProperties>) => {
    return new Promise((resolve, reject) => {
        result.value.saveAsync(result => {
            if (result.status === Office.AsyncResultStatus.Succeeded) {
                resolve(result);
            } else {
                reject(result);
            }
        });
    });
}