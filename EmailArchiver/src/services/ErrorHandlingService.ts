import {Notification} from "../interfaces/CustomInterfaces";
import {createNotification} from "../common/Utils";
import {
    HTTP_STATUS_CODE_400,
    HTTP_STATUS_CODE_401,
    HTTP_STATUS_CODE_403,
    HTTP_STATUS_CODE_404,
    HTTP_STATUS_CODE_409,
    HTTP_STATUS_CODE_413,
    HTTP_STATUS_CODE_422,
    NOTIFICATION_DURATION
} from "../common/Constants";

/**
 * Creates notification for Alfresco server errors
 */
export const handleServerErrors = (responseStatus,
                                   setNotification: (notification: Notification, durationInSeconds: number) => void) => {
    if (responseStatus && responseStatus.startsWith("5")) {
        const message = "Problem with Alfresco API";
        setNotification(createNotification(message), NOTIFICATION_DURATION);
    }
}

/**
 * Creates notification for client errors - auth, formatting, not existing
 */
export const handleClientErrors = (responseStatus,
                                   setNotification: (notification: Notification, durationInSeconds: number) => void,
                                   setLoggedIn: (isLoggedIn: boolean) => void) => {
    if (responseStatus && responseStatus.startsWith("4")) {
        let message: string;
        switch (responseStatus) {
            case HTTP_STATUS_CODE_400:
                message = "Problem with format of request";
                break;
            case HTTP_STATUS_CODE_401:
                message = "Problem with authentication";
                setLoggedIn(false);
                break;
            case HTTP_STATUS_CODE_403:
                message = "Problem with authorization";
                break;
            case HTTP_STATUS_CODE_404:
                message = "Problem with not existing resource";
                break;
            case HTTP_STATUS_CODE_409:
                message = "Problem with already existing node";
                break;
            case HTTP_STATUS_CODE_413:
                message = "Problem with too much large file";
                break;
            case HTTP_STATUS_CODE_422:
                message = "Problem with invalid characters for node name";
                break;
            default:
                message = "Problem with something unknown";
        }

        setNotification(createNotification(message), NOTIFICATION_DURATION);
    }
}

/**
 * Creates notification for general internet error
 */
export const handleNoInternetError = (isNetworkError,
                                      setNotification: (notification: Notification, durationInSeconds: number) => void) => {
    if (isNetworkError) {
        const message = "Problem with reaching to Alfresco API";
        setNotification(createNotification(message), NOTIFICATION_DURATION);
    }
}

/**
 * Aggregates all error handlers
 */
export const handleErrors = (error,
                             setNotification: (notification: Notification, durationInSeconds: number) => void,
                             setLoggedIn: (isLoggedIn: boolean) => void) => {
    const isNetworkError = error.message === "Network Error";
    handleNoInternetError(isNetworkError, setNotification);

    const responseStatus: string = error.response.status.toString();
    if (responseStatus) {
        handleClientErrors(responseStatus, setNotification, setLoggedIn);
        handleServerErrors(responseStatus, setNotification);
    }
}
