import * as React from "react";
// images references in the manifest
import "../../assets/icon-16.png";
import "../../assets/icon-32.png";
import "../../assets/icon-80.png";
import {FontIcon, MessageBarType} from "@fluentui/react";
import {removeToken} from "../api/authenticationApi";
import {Notification} from "../interfaces/CustomInterfaces";
import {NOTIFICATION_DURATION} from "../common/Constants";
import {createNotification} from "../common/Utils";

export interface CurrentMailProps {
    setCurrentMail: (currentEmail: string) => void;
    setLoggedIn: (isLoggedIn: boolean) => void;
    setNotification: (notification: Notification, durationInSeconds?: number) => void;
}

export interface CurrentMailState {
    mailSubject: string;
}

export default class CurrentMail extends React.Component<CurrentMailProps, CurrentMailState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            mailSubject: Office.context.mailbox.item.subject
        };
    }

    componentDidMount() {
        Office.context.mailbox.addHandlerAsync(
            Office.EventType.ItemChanged, this.onItemChange
        );
    }

    handleSignOut = async () => {
        removeToken();
        this.props.setLoggedIn(false);
        this.props.setNotification(createNotification("Successfully logged out", MessageBarType.success), NOTIFICATION_DURATION);
    }

    /**
     * Reacts to change of item (email) with saving subject of current email
     */
    onItemChange = () => {
        const emailSubject = Office.context.mailbox.item.subject;
        this.setState({mailSubject: emailSubject});
        this.props.setCurrentMail(emailSubject);
    }

    render() {
        const {mailSubject} = this.state;
        const {} = this.props;

        return (
            <div className={"current-mail"}>
                <b> {mailSubject}</b>
                <FontIcon className={"logout not-selectable"} iconName={"SignOut"} onClick={this.handleSignOut}/>
            </div>
        );
    }
}
