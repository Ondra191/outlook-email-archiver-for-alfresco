import * as React from "react";
import {obtainToken} from "../api/authenticationApi";
import {PrimaryButton, TextField} from "@fluentui/react";
import {Notification} from "../interfaces/CustomInterfaces";
import {handleErrors} from "../services/ErrorHandlingService";

export interface LoginProps {
    setLoggedIn: (isLoggedIn: boolean) => void;
    setNotification: (notification: Notification, durationInSeconds?: number) => void;
}

export interface LoginState {
    usernameInput: string;
    usernameError: string;
    passwordInput: string;
    passwordError: string;
}

export default class Login extends React.Component<LoginProps, LoginState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            usernameInput: "",
            usernameError: "",
            passwordInput: "",
            passwordError: ""
        };
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState(prevState => {
            return {...prevState, [name]: value};
        });
    }

    handleClick = (event) => {
        this.executeLogin().catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        });
    }

    handleEnterKeyDown = (e) => {
        if (e.keyCode === 13) {
            this.handleClick(e);
        }
    }

    /**
     * Checks username and password inputs and tries to authenticate the user with Alfresco
     */
    executeLogin = async () => {
        const username = this.state.usernameInput;
        const password = this.state.passwordInput;
        const usernameErrorMessage = username ? "" : "You must enter username";
        const passwordErrorMessage = password ? "" : "You must enter password";
        this.setState({usernameError: usernameErrorMessage, passwordError: passwordErrorMessage});

        if (username && password) {
            await obtainToken(username, password)
            this.props.setLoggedIn(true);
        }
    }

    render() {
        const {usernameError, passwordError} = this.state;
        const {} = this.props;

        return (
            <div className="login">
                <TextField
                    className={"username-input"}
                    name="usernameInput"
                    label="Alfresco username"
                    onChange={this.handleChange}
                    onKeyDown={this.handleEnterKeyDown}
                    errorMessage={usernameError}
                    autoFocus={true}
                />
                <TextField
                    className={"password-input"}
                    name="passwordInput"
                    label="Alfresco password"
                    onChange={this.handleChange}
                    onKeyDown={this.handleEnterKeyDown}
                    errorMessage={passwordError}
                    type="password"
                    canRevealPassword
                    revealPasswordAriaLabel="Show password"
                />
                <PrimaryButton
                    text="Login"
                    onClick={this.handleClick}
                />
            </div>
        );
    }
}
