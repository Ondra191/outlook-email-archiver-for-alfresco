import * as React from "react";
import {MessageBar, MessageBarType, Spinner, SpinnerSize} from "@fluentui/react";
// images references in the manifest
import "../../assets/icon-16.png";
import "../../assets/icon-32.png";
import "../../assets/icon-80.png";
/* global Button, Header, HeroList, HeroListItem, Progress */
import {isValidToken} from "../api/authenticationApi";
import {Notification} from "../interfaces/CustomInterfaces";
import Folders from "./Folders";
import {removeNotification} from "../common/Utils";
import Login from "./Login";
import {SECOND_IN_MILLISECONDS} from "../common/Constants";
import Timeout = NodeJS.Timeout;

export interface AppProps {
    isOfficeInitialized: boolean;
}

export interface AppState {
    notification: Notification;
    isLoggedIn: boolean;
    currentFolder: string;
    currentMail: string;
    notificationTimeout: Timeout | number | undefined;
}

export default class App extends React.Component<AppProps, AppState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            currentMail: "",
            isLoggedIn: null,
            currentFolder: "/",
            notification: {
                text: "",
                type: MessageBarType.success
            },
            notificationTimeout: undefined
        }
    }

    async componentDidMount() {
        const status = await isValidToken().catch(e => {
            this.setState({isLoggedIn: false});
        });

        if (status >= 200 && status < 300) {
            this.setState({isLoggedIn: true});
        }
    }

    setCurrentMail = (currentMail: string) => {
        this.setState({currentMail: currentMail});
    }

    setLoggedIn = (isLoggedIn: boolean) => {
        this.setState({isLoggedIn: isLoggedIn});
    }

    setNotification = (notification: Notification, durationInSeconds: number = -1) => {
        let timeout = null;
        if (durationInSeconds != -1) {
            timeout = () => this.notificationShowTimeout(durationInSeconds);
        }

        if (typeof this.state.notificationTimeout === "number") {
            clearTimeout(this.state.notificationTimeout);
        }

        this.setState({notification: notification}, timeout);
    }

    /**
     * Turns off visibility of notification after given seconds
     */
    notificationShowTimeout = (seconds: number) => {
        const timeout = setTimeout(() => {
            this.setState(prevState => {
                return {
                    notification: {...prevState.notification, text: ""},
                    notificationTimeout: undefined
                };
            })
        }, seconds * SECOND_IN_MILLISECONDS);
        this.setState({notificationTimeout: timeout});
    }

    render() {
        const {notification, currentMail} = this.state;
        const {isOfficeInitialized} = this.props;

        if (!isOfficeInitialized || this.state.isLoggedIn == null) {
            return (
                <div className="centralize">
                    <Spinner
                        size={SpinnerSize.large}
                        label={"Waiting for initialization of Add-in"}/>
                </div>
            );
        }

        return (
            <div>
                {notification.text && <MessageBar
                    className={"message-bar"}
                    messageBarType={this.state.notification.type}
                    isMultiline={false}
                    dismissButtonAriaLabel="Close"
                    onDismiss={() => this.setState({notification: removeNotification()})}
                >
                    {this.state.notification.text}
                </MessageBar>}

                {!this.state.isLoggedIn && <Login
                    setLoggedIn={this.setLoggedIn}
                    setNotification={this.setNotification}
                />}

                {this.state.isLoggedIn && <Folders
                    setCurrentMail={this.setCurrentMail}
                    currentMail={currentMail}
                    setLoggedIn={this.setLoggedIn}
                    setNotification={this.setNotification}
                />}
            </div>
        );
    }
}
