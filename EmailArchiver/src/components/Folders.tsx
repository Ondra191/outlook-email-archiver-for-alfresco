import * as React from "react";
import {createFolder, getFolderChildren, removeNode, renameNode} from "../api/NodesApi";
import {Node} from "../interfaces/AlfrescoInterfaces";
import {NodeIdentifier, Notification} from "../interfaces/CustomInterfaces";
import {
    Breadcrumb,
    CommandBar,
    DetailsList,
    DetailsListLayoutMode,
    FontIcon,
    IColumn,
    MessageBarType,
    Selection,
    SelectionMode,
    TextField
} from "@fluentui/react";
import ArchiveOptions from "./ArchiveOptions";
import {createNotification} from "../common/Utils";
import CurrentMail from "./CurrentMail";
import {handleErrors} from "../services/ErrorHandlingService";
import {DEFAULT_NODE_ROOT_ID, DEFAULT_NODE_ROOT_NAME, INPUT_REGEX, NOTIFICATION_DURATION} from "../common/Constants";

export interface FoldersProps {
    currentMail: string;
    setLoggedIn: (isLoggedIn: boolean) => void;
    setNotification: (notification: Notification, durationInSeconds?: number) => void;
    setCurrentMail: (currentEmail: string) => void;
}

export interface FoldersState {
    currentPath: NodeIdentifier[];
    nodes: Node[];
    foldersShown: boolean;
    nodeNameInput: string;
    nodeNameError: string;
    selectedNode?: Node;
    archivingToAlfresco: boolean;
}

export default class Folders extends React.Component<FoldersProps, FoldersState> {
    private readonly selection: Selection;
    private readonly columns: IColumn[];
    private readonly commandBarButtons: () => any;

    constructor(props, context) {
        super(props, context);

        new Selection()
        this.selection = new Selection({
            onSelectionChanged: this.getSelectionDetails
        });

        this.commandBarButtons = () => {
            if (this.state.foldersShown) {
                return [
                    {
                        key: "Archive",
                        text: "Archive",
                        iconProps: {iconName: "Upload"},
                        onClick: this.handleArchiveClick,
                    },
                    {
                        key: "create",
                        text: "Create",
                        iconProps: {iconName: "Add"},
                        onClick: this.handleCreateClick
                    },
                    {
                        key: "rename",
                        text: "Rename",
                        iconProps: {iconName: "Rename"},
                        onClick: this.handleRenameClick
                    },
                    {
                        key: "remove",
                        text: "Remove",
                        iconProps: {iconName: "Remove"},
                        onClick: this.handleRemoveClick
                    }
                ]
            } else {
                return [
                    {
                        key: "folders",
                        text: "Back to folders",
                        iconProps: {iconName: "FabricFolder"},
                        onClick: this.handleArchiveClick
                    }
                ]
            }
        }

        this.columns = [
            {
                key: "column1",
                name: "Document",
                iconName: "Document",
                isIconOnly: true,
                minWidth: 16,
                maxWidth: 16,
                columnActionsMode: 0,
                onRender: (item: Node) => {
                    const icon = item.isFolder ? "FabricFolderFill" : "Page";
                    return <FontIcon iconName={icon}/>;
                },
            },
            {
                key: "column2",
                name: "Nodes",
                fieldName: "name",
                minWidth: 100,
                data: "string",
                isPadded: true,
                columnActionsMode: 0
            }
        ];

        this.state = {
            nodes: [],
            archivingToAlfresco: false,
            foldersShown: true,
            nodeNameInput: "",
            nodeNameError: "",
            selectedNode: null,
            currentPath: [{
                id: DEFAULT_NODE_ROOT_ID,
                name: DEFAULT_NODE_ROOT_NAME
            }]
        };
    }

    async componentDidMount() {
        let children = await getFolderChildren(DEFAULT_NODE_ROOT_ID).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        });

        if (children) {
            this.setState({nodes: children});
        }
    }

    setArchivingToAlfresco = (archivingToAlfresco: boolean) => {
        this.setState({archivingToAlfresco: archivingToAlfresco});
    }

    addNodes: (newNodes: Node[]) => void = (newNodes: Node[]) => {
        this.setState(prevState => {
            return {nodes: [...prevState.nodes, ...newNodes]};
        });
    }

    /**
     * Switches between nodes file structure and archiving options
     */
    handleArchiveClick = () => {
        if (!this.state.archivingToAlfresco) {
            this.setState(prevState => {
                return {foldersShown: !prevState.foldersShown};
            });
        } else {
            this.props.setNotification(createNotification("Wait for finishing of archiving", MessageBarType.warning), NOTIFICATION_DURATION);
        }
    }

    /**
     * Creates folder
     */
    handleCreateClick = async () => {
        const parentFolder = this.getCurrentFolder();
        const newName = this.state.nodeNameInput;

        const newNode: Node | void = await createFolder(parentFolder.id, newName).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        });

        let children = await getFolderChildren(this.getCurrentFolder().id).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        });

        if (!newNode || !children) {
            return;
        }

        this.setState({nodes: children}, () => {
            this.getSelectionDetails();
        });
    }

    /**
     * Renames node
     */
    handleRenameClick = async () => {
        const nodeId = this.state.selectedNode.id;
        const newName = this.state.nodeNameInput;

        const renamedNode: Node | void = await renameNode(nodeId, newName).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        });

        if (!renamedNode) {
            return;
        }

        this.setState(prevState => {
            const newNodes = prevState.nodes.map(v => {
                if (renamedNode && v.id === nodeId) {
                    v.name = renamedNode.name;
                }
                return v;
            })
            return {nodes: newNodes};
        })
    }

    /**
     * Removes node
     */
    handleRemoveClick = async () => {
        const nodeId = this.state.selectedNode.id;

        const statusCode: number | void = await removeNode(nodeId).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        });

        if (!statusCode) {
            return;
        }

        this.setState(prevState => {
            const newNodes = prevState.nodes.filter(v => v.id !== nodeId);
            return {nodes: newNodes};
        }, () => {
            this.getSelectionDetails();
        })
    }

    handleBreadCrumbClick = async (ev?: React.MouseEvent<HTMLElement>, item?: NodeIdentifier) => {
        await this.getRequestedChildren(true, item);
    }

    handleItemInvoke = async (item?: Node) => {
        if (item.isFolder) {
            await this.getRequestedChildren(false, item);
        }
    }

    handleNodeNameInput = (event) => {
        let value = event.target.value;
        value = value.replace(INPUT_REGEX, "");
        this.setState({nodeNameInput: value});
    }

    getSelectionDetails = () => {
        const selectionCount = this.selection.getSelectedCount();
        const node = selectionCount === 1 ? this.selection.getSelection()[0] : null;
        const nodeName = selectionCount === 1 ? (node as Node).name : "";
        this.setState({selectedNode: node as Node, nodeNameInput: nodeName});
    }

    getCurrentFolder = () => {
        let lastIndex = this.state.currentPath.length - 1;
        return this.state.currentPath[lastIndex];
    }

    getBreadcrumbItems = () => {
        if (!this.state.currentPath) {
            return [];
        }
        let items = [];

        for (const nodeIdentifier of this.state.currentPath) {
            let item = {
                text: nodeIdentifier.name,
                key: nodeIdentifier.id,
                id: nodeIdentifier.id,
                name: nodeIdentifier.name,
                onClick: this.handleBreadCrumbClick,
                isCurrentItem: false
            }
            items.push(item);
        }
        const lastIndex = items.length - 1;
        items[lastIndex].isCurrentItem = true;

        return items;
    }

    /**
     * Gets nodes of given folder and updates breadcrumb and details list accordingly
     */
    getRequestedChildren = async (isFromBreadcrumb: boolean, item: NodeIdentifier | Node) => {
        const children = await getFolderChildren(item.id).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
        }) ?? Node[0];

        const newNodeIdentifier: NodeIdentifier = {
            id: item.id,
            name: item.name
        }


        this.setState(prevState => {
            let newCurrentPath = [];
            if (isFromBreadcrumb) {
                for (const nodeIdentifier of prevState.currentPath) {
                    newCurrentPath.push(nodeIdentifier);
                    if (nodeIdentifier.id === item.id) {
                        break;
                    }
                }
            } else {
                newCurrentPath = [...prevState.currentPath, newNodeIdentifier];
            }

            return {
                nodes: children,
                currentPath: newCurrentPath,
                selectedNode: null,
                nodeNameInput: ""
            };
        }, () => {
            this.selection.setAllSelected(false);
        });
    }

    render() {
        const {nodes, foldersShown, nodeNameInput, nodeNameError} = this.state;
        const {setNotification, setLoggedIn, currentMail, setCurrentMail} = this.props;

        return (
            <div className={"main-container"}>

                <div className={"sticky"}>
                    <CurrentMail
                        setCurrentMail={setCurrentMail}
                        setLoggedIn={setLoggedIn}
                        setNotification={setNotification}
                    />
                    <CommandBar
                        className={"command-bar"}
                        items={this.commandBarButtons()}
                    />
                    {foldersShown && <div>
                        <TextField
                            className={"node-input"}
                            maxLength={1000}
                            label="Set node name:"
                            onChange={this.handleNodeNameInput}
                            value={nodeNameInput}
                            errorMessage={nodeNameError}
                        />
                        <Breadcrumb
                            className={"bread-crumb"}
                            items={this.getBreadcrumbItems()}
                            dividerAs={() => <span aria-hidden="true" style={{cursor: "pointer", padding: 5}}>/</span>}
                            ariaLabel="Breadcrumb with items rendered as links"
                            overflowAriaLabel="More links"
                        />
                    </div>}
                </div>

                {foldersShown && <DetailsList
                    className={"not-selectable"}
                    items={nodes}
                    columns={this.columns}
                    selection={this.selection}
                    setKey="single"
                    getKey={(item) => {
                        return item.id;
                    }}
                    selectionPreservedOnEmptyClick={true}
                    selectionMode={SelectionMode.single}
                    layoutMode={DetailsListLayoutMode.justified}
                    onItemInvoked={this.handleItemInvoke}
                />}

                {!foldersShown && <ArchiveOptions
                    currentMail={currentMail}
                    addNodes={this.addNodes}
                    setArchivingToAlfresco={this.setArchivingToAlfresco}
                    setLoggedIn={setLoggedIn}
                    setNotification={setNotification}
                    folder={this.getCurrentFolder()}
                />}

            </div>
        );
    }
}

