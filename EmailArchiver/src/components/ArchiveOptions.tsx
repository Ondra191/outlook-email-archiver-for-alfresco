import * as React from "react";
import {
    Checkbox,
    FontIcon,
    MessageBarType,
    PrimaryButton,
    Spinner,
    SpinnerSize,
    Stack,
    TextField
} from "@fluentui/react";
import {
    AttachmentCheckboxIdentifier,
    NodeIdentifier,
    Notification,
    SendProperties
} from "../interfaces/CustomInterfaces";
import {createArchiveEmailConfig, saveAttachments, saveEmail} from "../services/ArchiveService";
import {createNotification, removeNotification} from "../common/Utils";
import {getPersistentEmailProperty} from "../services/MailPropertiesService";
import {Node} from "../interfaces/AlfrescoInterfaces";
import {
    ALREADY_SAVED_MESSAGE,
    ATTACHMENTS_PERSISTENT_KEY,
    CLOUD_ATTACHMENT_TYPE,
    EMAIL_PERSISTENT_KEY,
    INPUT_REGEX,
    NOTIFICATION_DURATION
} from "../common/Constants";
import {handleErrors} from "../services/ErrorHandlingService";

export interface ArchiveOptionsProps {
    folder: NodeIdentifier;
    setLoggedIn: (isLoggedIn: boolean) => void;
    setNotification: (notification: Notification, durationInSeconds?: number) => void;
    addNodes: (newNodes: Node[]) => void;
    setArchivingToAlfresco: (archivingToAlfresco: boolean) => void;
    currentMail: string;
}

export interface ArchiveOptionsState {
    emailNameError: string;
    emailNameInput: string;
    sendEmail: boolean;
    emailSaved: boolean;
    attachmentsToSend: AttachmentCheckboxIdentifier[];
    sendProperties: SendProperties;
    isArchiving: boolean;
}

export default class ArchiveOptions extends React.Component<ArchiveOptionsProps, ArchiveOptionsState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            isArchiving: false,
            emailNameError: "",
            emailNameInput: "Email",
            sendEmail: true,
            emailSaved: false,
            attachmentsToSend: [],
            sendProperties: {
                sendSubject: true,
                sendFrom: true,
                sendTo: true,
                sendToAll: true,
                sendTime: true,
            }
        };
    }

    async componentDidMount() {
        this.initializeData().catch(e => handleErrors(e, this.props.setNotification, this.props.setLoggedIn));
    }

    componentDidUpdate(prevProps: Readonly<ArchiveOptionsProps>, prevState: Readonly<ArchiveOptionsState>, snapshot?: any) {
        if (prevProps.currentMail !== this.props.currentMail) {
            this.initializeData().catch(e => handleErrors(e, this.props.setNotification, this.props.setLoggedIn));
        }
    }

    handleEmailNameChange = (event) => {
        let value: string = event.target.value;
        value = value.replace(INPUT_REGEX, "");
        this.setState({emailNameInput: value});
    }

    handleEmailCheckbox = (event) => {
        const checked = event.target.checked;
        this.setState({sendEmail: checked});
    }

    handleAttachmentsCheckboxes = (event, checked?: boolean) => {
        const id = event.target.name;

        this.setState(prevState => {
            const newAttachmentsToSend = prevState.attachmentsToSend.map(v => {
                if (v.id === id) {
                    v.checked = checked;
                }
                return v;
            });
            return {...prevState, attachmentsToSend: newAttachmentsToSend};
        });
    }

    handlePropertiesCheckboxes = (event) => {
        const name = event.target.name;
        const checked = event.target.checked;

        this.setState(prevState => {
            return {sendProperties: {...prevState.sendProperties, [name]: checked}};
        });
    }

    /**
     * Starts archiving checked attachments/email and informs user via notification about outcome
     */
    handleClick = async () => {
        if (!this.state.sendEmail && this.state.attachmentsToSend.length === 0) {
            return;
        }

        let noErrors = true;

        this.setState({isArchiving: true});
        this.props.setArchivingToAlfresco(true);
        this.props.setNotification(removeNotification());

        const sendProperties = this.state.sendProperties;
        const parentFolder = this.props.folder.id;
        let newNodes: Node[] = [];

        if (this.state.sendEmail) {
            const emailConfig = createArchiveEmailConfig(this.state.emailNameInput, sendProperties);
            const emailNode = await saveEmail(parentFolder, emailConfig).catch(e => {
                handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
                noErrors = false;
            });

            if (emailNode) {
                newNodes.push(emailNode);
            }
        }

        const attachmentIds = this.getIdOfCheckedAttachments();
        const attachmentNodes = await saveAttachments(parentFolder, sendProperties, attachmentIds).catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn)
            noErrors = false;
        });

        if (attachmentNodes) {
            newNodes = newNodes.concat(attachmentNodes);
        }

        this.checkIfAlreadySavedEmail().catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
            noErrors = false;
        });
        this.checkIfAlreadySavedAttachments().catch(e => {
            handleErrors(e, this.props.setNotification, this.props.setLoggedIn);
            noErrors = false;
        });

        this.setState({isArchiving: false});
        this.props.addNodes(newNodes);
        this.props.setArchivingToAlfresco(false);
        if (noErrors && (this.state.sendEmail || (attachmentIds && attachmentIds.length > 0))) {
            this.props.setNotification(createNotification("Saving finished successfully", MessageBarType.success), NOTIFICATION_DURATION);
        }
    }

    /**
     * Returns id of wanted attachments
     */
    getIdOfCheckedAttachments = () => {
        return this.state.attachmentsToSend.filter(v => v.checked).map(v => v.id);
    }

    initializeData = async () => {
        this.checkIfAlreadySavedEmail();
        this.getAttachmentsCheckboxes();
    }

    /**
     * Checks already saved email and updates him in state
     */
    checkIfAlreadySavedEmail = async () => {
        const isSaved = await getPersistentEmailProperty(EMAIL_PERSISTENT_KEY);
        this.setState({emailSaved: isSaved, sendEmail: !isSaved});
    }

    /**
     * Checks already saved attachments and updates them in state
     */
    checkIfAlreadySavedAttachments = async (attachments = this.state.attachmentsToSend) => {
        let newAttachmentsToSend = [];

        for (const attachment of attachments) {
            attachment.isSaved = await getPersistentEmailProperty(ATTACHMENTS_PERSISTENT_KEY + attachment.id);
            attachment.checked = !attachment.isSaved;
            if (attachment.attachmentType === CLOUD_ATTACHMENT_TYPE) {
                attachment.checked = false;
            }
            newAttachmentsToSend.push(attachment);
        }

        this.setState({attachmentsToSend: newAttachmentsToSend});
    }

    /**
     * Returns data for UI attachment checkboxes (basic info + if saved to Alfresco already)
     */
    getAttachmentsCheckboxes = async () => {
        const attachmentsCheckboxes = Office.context.mailbox.item.attachments.map(v => {
            let checked = true;
            let label = "";
            let disabled = false;
            if (v.attachmentType === CLOUD_ATTACHMENT_TYPE) {
                checked = false;
                label = " (Link to cloud file)"
                disabled = true;
            }

            let attachmentCheckboxIdentifier: AttachmentCheckboxIdentifier = {
                checked: checked,
                id: v.id,
                attachmentType: v.attachmentType,
                name: v.name + label,
                disabled: disabled,
                isSaved: false,
            };
            return attachmentCheckboxIdentifier;
        });

        await this.checkIfAlreadySavedAttachments(attachmentsCheckboxes);
    }

    render() {
        const {
            sendEmail,
            sendProperties,
            emailNameError,
            emailNameInput,
            attachmentsToSend,
            emailSaved,
            isArchiving
        } = this.state;
        const {folder} = this.props;

        return (
            <div className="archive-options">
                <Stack
                    horizontalAlign={"start"}
                    verticalAlign={"space-around"}
                    tokens={{childrenGap: 5}}
                >

                    <div className={"sticky-folder"}>
                        <FontIcon className={"folder-icon"} iconName={"FabricFolderFill"}/>
                        <h1>{folder.name}</h1>
                    </div>

                    <h2>Email text:</h2>
                    <Checkbox label={(emailSaved ? ALREADY_SAVED_MESSAGE : "") + "Save Email body"}
                              name="sendEmail" checked={sendEmail} onChange={this.handleEmailCheckbox}/>
                    <TextField
                        className={"email-input"}
                        maxLength={1000}
                        name="usernameInput"
                        label="Set email name:"
                        onChange={this.handleEmailNameChange}
                        value={emailNameInput}
                        errorMessage={emailNameError}
                    />

                    {attachmentsToSend && attachmentsToSend.length > 0 && <h2>Email attachments:</h2>}
                    {attachmentsToSend.map(v => {
                        return <Checkbox
                            name={v.id}
                            label={(v.isSaved ? ALREADY_SAVED_MESSAGE : "") + v.name}
                            key={v.id}
                            checked={v.checked}
                            disabled={v.disabled}
                            onChange={this.handleAttachmentsCheckboxes}
                        />
                    })}

                    <h2>Email properties:</h2>
                    <p>Attach these properties to saved email and attachments:</p>
                    <Checkbox label="Subject" name="sendSubject" checked={sendProperties.sendSubject}
                              onChange={this.handlePropertiesCheckboxes}/>
                    <Checkbox label="Author" name="sendFrom" checked={sendProperties.sendFrom}
                              onChange={this.handlePropertiesCheckboxes}/>
                    <Checkbox label="Addressee" name="sendTo" checked={sendProperties.sendTo}
                              onChange={this.handlePropertiesCheckboxes}/>
                    <Checkbox label="All recipients" name="sendToAll" checked={sendProperties.sendToAll}
                              onChange={this.handlePropertiesCheckboxes}/>
                    <Checkbox label="Date and time of sending" name="sendTime" checked={sendProperties.sendTime}
                              onChange={this.handlePropertiesCheckboxes} className={"last-checkbox"}/>

                    <div className={"wrapper"}>
                        {isArchiving && <Spinner
                            className={"spinner"}
                            size={SpinnerSize.large}
                            label={"Waiting for archiving to complete"}
                        />}

                        <PrimaryButton
                            text="Save selected"
                            onClick={this.handleClick}
                        />

                    </div>
                </Stack>
            </div>
        );
    }
}
