# Outlook email archiver for alfresco

Outlook plugin for sending emails and attachments to Alfresco. 

- Authentication to Alfresco server


- Managing Alfresco nodes in plugin


- Allows choosing which email headers to send as metadata

## Project structure

- **Alfresco** - Docker images for running Alfresco
- **Email archiver** - Outlook plugin in JS React

## Login 

Alfresco has default admin user.

- **Username** - admin
- **Password** - admin
