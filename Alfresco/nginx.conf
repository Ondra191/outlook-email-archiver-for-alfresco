worker_processes 1;

events {
    worker_connections  1024;
}

http {
    server {
            listen *:8080;
            server_name localhost;

            add_header "Access-Control-Allow-Origin"  * always;
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' *;
            add_header "Access-Control-Allow-Headers" "Authorization, Origin, X-Requested-With, Content-Type, Accept";

            if ($request_method = 'OPTIONS') {
                return 204;
            }

            client_max_body_size 0;
            proxy_pass_request_headers on;
            proxy_pass_header Set-Cookie;

            # External settings, do not remove
            #ENV_ACCESS_LOG

            proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
            proxy_redirect off;
            proxy_buffering off;
            proxy_set_header Host            $host:$server_port;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass_header Set-Cookie;

            # Protect access to SOLR APIs
            location ~ ^(/.*/service/api/solr/.*)$ {return 403;}
            location ~ ^(/.*/s/api/solr/.*)$ {return 403;}
            location ~ ^(/.*/wcservice/api/solr/.*)$ {return 403;}
            location ~ ^(/.*/wcs/api/solr/.*)$ {return 403;}

            location ~ ^(/.*/proxy/.*/api/solr/.*)$ {return 403 ;}
            location ~ ^(/.*/-default-/proxy/.*/api/.*)$ {return 403;}

            # Prometheus settings, do not remove
            #PROMETHEUS_LOCATION

            location / {
                proxy_pass http://alfresco:8080;
            }

            location /alfresco/ {
                proxy_pass http://alfresco:8080;

                # If using external proxy / load balancer (for initial redirect if no trailing slash)
                absolute_redirect off;
            }

            # Share settings, do not remove
            location /share/ {
                proxy_pass http://share:8080;
                absolute_redirect off;
            }

            # ADW settings, do not remove
            #ADW_LOCATION

            # Sync service settings, do not remove
            #SYNCSERVICE_LOCATION
        }

    #server {
	#    listen 80;
    #    server_name localhost;
#
	#    #location / {
    #    #             return 301 https://$host$request_uri;
    #    #        }
#
#
    #    location /.well-known/acme-challenge/ {
    #        root /var/www/certbot;
    #    }
    #}
    #server {
    #    listen 443 ssl;
    #    server_name         localhost;
    #    ssl_certificate     /etc/nginx/ssl/cert.crt;
    #    ssl_certificate_key /etc/nginx/ssl/cert.key;
    #    #ssl_certificate /etc/letsencrypt/live/localhost/fullchain.pem;
    #    #ssl_certificate_key /etc/letsencrypt/live/localhost/privkey.pem;
    #    root      /usr/share/nginx/html;
    #    index index.html;
    #    error_log /var/log/nginx/ssl-error.log;
#
    #    location / {
    #           root  /usr/share/nginx/html;
    #           index index.html;
    #    }
    #}
}
